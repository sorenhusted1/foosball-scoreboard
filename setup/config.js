
/**
 * IMPORTANT: SUPPORTED BROWSERS
 * *************************************************************************************
 *
 * This is where you define the browser compatibility needed for your output!
 *
 * Set this to the absolute minimum you need to support. The newer the browser, the less
 * transpiling is needed. For a new-Chrome-only experience, you could deliver pure ES6
 * by setting this to "last 2 Chrome versions", for example.
 *
 * You can find examples on how to configure this here:
 * https://github.com/browserslist/browserslist#full-list
 *
 * @private
 * @type {string[]}
 */
const supportedBrowsers = ["last 2 versions", "not dead", "IE 11"];



/**
 * ALSO IMPORTANT: MINIMUM SUPPORTED JAVASCRIPT VERSION
 * *************************************************************************************
 *
 * While the code transpilation step makes sure your Javascript will run just about
 * anywhere, you might be able to optimize it further by specifying a minimum ECMA
 * version. The default (5) will work everywhere, but if your target browsers have full
 * support for ECMA6 or newer, you can set the version here. This is an important part
 * of the Terser uglify-plugin we're using, and will let you avoid unnecessary dumbing
 * down of your code.
 *
 * You might be thinking "why can't we do this automatically based on the browser
 * setting I just made?", and the answer is a resounding "because reasons" ... or maybe
 * look here: https://github.com/facebook/create-react-app/issues/4240#issuecomment-409880746
 *
 *
 * See https://github.com/terser-js/terser for info about how this is used, or look
 * inside the Javascript-task for more. If you're unsure, just leave this at 5.
 *
 * @private
 * @type {number}
 */
const ecmaTargetVersion = 5;




const configuration = {

    /** Set this to true to disable all minificaton during normal development. Normally you'd want to set this to "true". */
    disableMinificationDuringDevelopment: true,


    /** "folders" are where you define all the default paths for the tasks to use. These are pretty important. */
    folders: {
        /** The root is the path of the resources relative to the gulpfile. You shouldn't change this. */
        root: "./",

        /**
         * The sourceFolder is where you keep all your source files. Makes a certain kind of sense when
         * you think about it.
         */
        sourceFolder: "static/src/",

        /**
         * The targetFolder(s) is/are where you want your compiled assets to go. This path is relative to the
         * root, so you can easily add "../Webroot/static/" too, but remember that the more targetFolders you
         * have, the more junk you'll put on your harddrive. If you want a special setup for use in BrowserSync
         * or similar, there are much smarter ways than adding a billion targetPaths. ;-)
         */
        targetFolder: [
            "static/dist/"
        ],


        /**
         * This path is where Babel/Webpack will look for dynamically imported JS and CSS during runtime. This
         * path MUST correspond to the asset path on your website (this includes the initial slash!), or things
         * will break for you at some point. If you don't use dynamically imported code, don't worry about it.
         */
        dynamicJSImportSource: "/static/dist/js/"
    },



    /** This is where you define the setup for all the tasks. */
    tasks: {


        browsersync: {
            /** Enable BrowserSync? */
            enable: true,

            /** Enable the engine for parsing SHTML/SSI statements? */
            enableSSI: true,

            /** Which file should be used as the index? */
            indexFile: "index.shtml",

            /** Which directory should be used as the resource root? */
            baseDir: "./",

            /**
             * Routes are cool. This is where you can point BrowserSync to load various paths from different folders,
             * in case your setup is "weird", or you don't want a billion targetFolders (do you remember reading about
             * this? If not, go back and start from the top!). Examples of valid routes would be:
             *
             *  // Any request to "/js" should be loaded from the "../Webroot/js" folder instead.
             *    "/js": "../Webroot/js/"
             *
             *  // The same for CSS:
             *    "/css": "../Webroot/css/"
             *
             *  // Or you can just map a single file:
             *    "/SomeFile.txt": "../../../yo.doc"
             *
             * The possibilities are almost endless, and lets you create a pretty advanced setup without having more
             * than the one targetFolder. Neat, huh?
             *
             * The default here, "/static", ensures that "absolute" calls to static files are resolved correctly in
             * case of the user being inside a subfolder.
             */
            routes: {
                "/static": "static"
            },

            /** "port" is the port BrowserSync will listen on. You really should change this, ESPECIALLY if you're going to use service workers. */
            port: 3000,

            /** "uiPort" is the port BrowserSync will set its administration interface up on. */
            uiPort: 3001,

            /** Y'all want HTTPS or nah? */
            https: false,

            /** How many interactions should carry between different sessions on the same BrowserSync-server? */
            ghostMode: {
                clicks: true,
                forms: true,
                scroll: false
            },

            /** You can reduce startup time by setting this to "false" in case you're... you know, offline. */
            online: true,

            /**
             * You can setup BrowserSync to act as a proxy instead of a server. This lets you serve a local site
             * (from IIS or Visual Studio) to your local network for easier debugging and testing. Do this by
             * setting the target address here. The address must be without the protocol-prefix (`http://` or
             * `https://`). For example: "localhost:52456" or "frontline.dis-play.local"
             */
            proxy: "",

            /** These are the folders inside which we'll be watching for HTML-changes. Just because. */
            htmlFolders: [
                "./",
                "demo-html-folder/"
            ],

            /** Which files should be treated as HTML-files (with or without SSI-parsing)? */
            htmlFilePatterns: [
                "*.shtml",
                "*.html"
            ]
        },



        build: {
            /** Once a build is performed (during "build", "default" or similar tasks), these are the tasks that are run. */
            tasks: [
                "modernizr",
                "svg",
                "copyAssets",
                "scss",
                "serviceworkers",
                "javascript",
                "javascript:polyfills"
            ]
        },




        clean: {
            /** These tasks will be cleaned when the "clean" task is run. Their target folder(s) will be deleted. Simple as that. */
            tasks: [
                "svg",
                "copyAssets",
                "javascript",
                "scss"
            ]
        },


        copyAssets: {
            /**
             * These folders beneath the "global sourceFolder" will be copied 1:1 to the output. Very useful for fonts
             * and similar resources, but please don't do it for SVG-files.
             */
            sourceFolder: [
                "fonts",
                "img",
                "media"
            ],
            targetFolder: ".",
            watch: true,
            watchFor: "*.*"
        },


        javascript: {
            /** Where do we get the source? */
            sourceFolder: "js",

            /**
             * Exclude certain source files from the linter. If you add your own code to this, I will look for you,
             * I will find you, and I will kill you. Well, maybe not KILL... but don't do it.
             */
            excludeFromLint: [
                "vendor/**/*.js"
            ],

            /** Where does the code end up? */
            targetFolder: "js",


            /**
             * Each entry point will result in a separate payload. If you add more entries here, please be aware that
             * you'll probably need to configure the code splitting manually in order to get the results you need (see
             * "codeSplittingConfiguration" further down).
             *
             * If you're not up for that particular challenge, please only make one entry point, and then import your
             * code dynamically through that using the async-loader utilities or similar.
             *
             * The regular format is a flat array of specific entries, ie. ["main.js", "not-main.js"], which will result
             * in two payloads named "main.min.js" and "not-main.min.js", respectively. This should be more than enough
             * for a regular setup. However, you can also go a lot deeper (insert Leo DiCaprio JPEG here) (yes, I am teh
             * meme lord), like so:
             *
             * entryPoints: { // notice it's now an object and not an array!
             *     "hello": [
             *         "*.js",
             *         "../other-folder/some-other-file.js"
             *     ]
             * }
             *
             * This will generate a single payload called "hello.min.js", which will contain all the JS-files inside the
             * sourceFolder (but not below - it's a standard glob, so BE CAREFUL here!), as well as a file from a
             * completely different folder outside the "regular path". This could be very helpful if you have to code
             * up against a SiteCore Helix structure, for example. In this case, you'll want to add the relevant folders
             * to the `watchFolders` option further below, to force the watcher to keep an eye on ALL your files, not
             * just the ones in the regular source folder.
             */
            entryPoints: [
                "main.js"
            ],


            /**
             * The options you set here will be used directly for the "optimization" settings in Webpack. Normally you'd
             * want to leave this empty, but if you want fine-grained control of the code splitting procedures, this is
             * your access point.
             *
             * See https://webpack.js.org/configuration/optimization/ for more.
             */
            codeSplittingConfiguration: {},


            /**
             * This is the filename used for generated/split JS-chunks. You might need to tweak this to your specific
             * needs, depending on what kind of setup you're running.
             */
            chunkFilename: "chunk.[name].[chunkhash:6].js",


            /**
             * The output filename is where you determine what the output file will be called.
             */
            outputFilename: "[name].min.js",


            /**
             * Define which kind of source-map should be generated during a regular development build.
             *
             * See https://webpack.js.org/configuration/devtool/ for more.
             */
            devSourceMapMode: "cheap-module-eval-source-map",



            /** This is the transpiler. You like the transpiler. The transpiler is your friend. */
            babelPresetEnv: {
                modules: false,
                targets: {
                    browsers: supportedBrowsers
                }
            },

            // See the top of this file for more about this.
            ecmaTargetVersion,

            /** Do we watch for changes? */
            watch: true,

            /**
             * Why watch for SCSS-files? Because we're also watching the style-files directly relating to CSS-in-JS,
             * and when they are changed, we need to recompile the JS and not the SCSS itself. So... there.
             */
            watchFor: "**/*.(js|scss)",

            /** Which tasks should be run when a file changes? */
            watchRun: [
                "eslint",
                "javascript"
            ],

            /**
             * We'll keep a watch on these folders. If your JS also lives outside the "js" folder (see the documentation
             * for the entry points above), you should probably add the relevant folder here as well.
             */
            watchFolders: [
                "js",
                "scss/components/js"
            ],


            /**
             * CSS-in-JS configuration. The description of this could fill a book in itself, but start by reading
             * README.md to see what is possible with the compiler, and then change the options to suit your needs.
             */
            stylesInJS: {
                useDynamicIncludes: true,
                useStyleInjectionInDevelopment: false,
                staticFilename: "[name].css",
                dynamicFilename: "[name].[hash:6].css",
                outputFolderName: "components",
                cssModulesEnabled: false,
                classNamePatternDevelopment: "[path][name]__[local]--[hash:base64:6]",
                classNamePatternProduction: "[name]--[hash:base64:6]"
            },


            /**
             * We cache a polyfill-file locally so we have something to fall back on if the polyfill.io service
             * should be down unexpectedly (it has been known to happen).
             */
            polyfillFallbackFetcher: {
                /**
                 * "Arguments" are all sent along to the polyfill server without any modifications. You can see
                 * supported arguments for Polyfill.io here: https://polyfill.io/v2/docs/api
                 */
                arguments: {
                    /**
                     * These features should correspond to the ones you define in your master template for the
                     * "regular" polyfill service.
                     */
                    features: "default-3.6,fetch",

                    /**
                     * The "ua" defines the lowest supported browser (default is IE11). Be careful to test this
                     * if you change it, since some polyfills can actually break stuff on newer Chromes. If
                     * stuff breaks, you might be able to salvage the situation by using the "excludes" setting
                     * below.
                     */
                    ua: "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",

                    /**
                     * If a polyfill is acting up, and isn't absolutely necessary for the site to function, you
                     * can exclude it here. We remove the "classList" polyfill because our own classlist utility
                     * actually does much of this stuff already, and because it actually crashes on Chrome.
                     */
                    excludes: "Element.prototype.classList",

                    /**
                     * The name of the local callback the polyfill file should run when done.
                     */
                    callback: "loadMainScript"
                },
                url: "https://cdn.polyfill.io/v2/polyfill.min.js",
                targetFilename: "polyfillFallback.js",
                targetFolder: "js"
            }
        },


        modernizr: {
            filename: "modernizr.js",
            targetFolder: "js"
        },




        scss: {
            sourceFolder: "scss",
            targetFolder: "css",
            entryPoints: [
                "critical.scss",
                "main.scss"
            ],
            excludeFromLint: [
                "vendor/**/*.{,s}css"
            ],


            /**
             * The suffix is something we'll append to the filename to indicate it's a minified version. Thus, `main.css` becomes
             * `main.min.css` if you set this to `.min`. Whether or not you want to do that is entirely up to you.
             */
            outputFilenameSuffix: ".min",


            /**
             * The output file extension. You probably shouldn't change this.
             */
            outputExtension: ".css",


            /**
             * CSSNano makes things smaller. It's very good at it. You can find more about its options here:
             * https://cssnano.co/optimisations/
             */
            cssNanoConfig: {
                /**
                 * This stops the z-index optimization, which becomes a problem when the CSS is split across several
                 * files. Leave it off unless saving those bytes is a matter of life and death.
                 */
                zindex: false,

                /**
                 * This stops ident-optimization, which has to do with naming stuff like animations. Again, this is
                 * for code splitting reasons, and you should probably leave this off unless you know what you're
                 * doing. Otherwise, you'll just find your keyframes disappearing. ¯\_(ツ)_/¯
                 */
                reduceIdents: false,

                /**
                 * We don't want to optimize (remove) what the autoprefixer has done.
                 */
                autoprefixer: false
            },

            autoPrefixerConfig: {
                /** As with the JS, there's no reason to prefix code for browsers you're not going to support anyway. */
                browsers: supportedBrowsers,
                add: true
            },

            /**
             * Set this to "true" to group and sort media queries automatically. This will drastically reduce your
             * CSS file size, especially for large sites, but the sorter may have trouble dealing with your
             * particular code. If you experience weird specificity errors or anything you can't really explain,
             * try setting this to false.
             */
            groupMediaQueries: true,


            watch: true,
            watchFor: "*.scss",
            watchRun: [
                "stylelint",
                "scss"
            ],

            /**
             * We'll watch everything in the watch-folders, EXCEPT the styling for JS-components. Those should be
             * handled by the JS-task instead, since they'll be dragged through Webkit anyway.
             */
            watchFolders: [
                "scss",
                "!scss/components/js",
            ]
        },




        serviceworkers: {
            enable: false,

            sourceFolder: "serviceworkers",

            /**
             * We use a local file to keep track of which version of the service worker is currently
             * being deployed to production. This should be included in your version control, but please
             * make sure you only bump its version once per commit, even though you might be building
             * (and thus version-bumping) many times per session.
             *
             * The bumping is automatic. "The bumping" makes me think of "Der Humpink".
             * https://www.youtube.com/watch?v=kUbGcRJUDu0
             */
            versionFile: "serviceworker-version.json",

            /**
             * The target folder should ideally point into the root of your output destination.
             */
            targetFolder: "../../",

            watch: true,
            watchFor: "*.*",
            watchRun: [
                "eslint",
                "serviceworkers"
            ]
        },


        /**
         * The SXA-task is for synchronizing static assets such as javascript and css with the media library in
         * SiteCore SXA. This is experimental, and should not be attempted by the faint of heart. Normally this
         * task is disabled, which will hide it from the task list, but you can enable it if you need it. In that
         * case, my heart goes out to you.
         *
         * If you DO enable it, make sure you change the relevant settings in the `server`-section.
         *
         * Also: BE AWARE! SiteCore WILL mess up your file names, so you need to change your output paths to
         * NOT contain suffixes:
         *    "main.min.css" should be "main.css"
         *    "modal.77f27e.css" should be "modal.css"
         *
         * This is something you need to configure the various tasks to do, so please start there before you start
         * uploading your code (notably "outputFilename" in the JS-task, and "outputFilenameSuffix" in the SCSS-task).
         */
        sxa: {
            enable: false,


            /**
             * You probably shouldn't change this. It'd be better to exclude files using the patterns
             * further below.
             */
            watchFor: "*.*",


            /**
             * This is how many milliseconds we'll wait from the last registered file action until we start
             * synchronizing with the server. This is to avoid sending too much data down the line. Or up.
             */
            actionDebounceDelay: 500,


            /**
             * Since SiteCore has the BRILLIANT habit of stripping off the extension when we upload files
             * into the media library, we might need to transform some file names before syncing them. Any
             * file matching an extension on this list will be renamed to contain the extension as part of
             * the filename. Like so:
             *
             * "helvetica.ttf" becomes "helvetica-ttf.ttf"
             * "helvetica.woff" becomes "helvetica-woff.woff"
             *
             * ... and so on. If we DON'T do this, files with matching file names (but different extensions)
             * will just overwrite each other, and that would be extremely painful. For you.
             */
            transformFilenamesForExtensions: [
                /\.ttf$/,
                /\.woff$/,
                /\.woff2$/,
                /\.eot$/,
            ],


            /**
             * Files matching these patterns will be treated as templates, not as asset files. The upload
             * methodology there is a little different, y'see.
             */
            templateFilePatterns: [
                /\.html$/
            ],


            /**
             * Files matching these patterns will not be synchronized at all. Like, at all.
             */
            ignorePatternsForAllOperations: [
                /\.css\.map$/,
                /\.temp$/,
                /\.tmp$/
            ],


            /**
             * Files matching these patterns (OR any of the ones in `ignorePatternsForAllOperations`) will
             * not be deleted from the server.
             */
            ignorePatternsForDeleteOperations: [
                /\.html$/
            ],


            /**
             * This is where you define the server configuration.
             */
            server: {
                /**
                 * The hostname is pretty important. This is where the script will locate your SiteCore-installation.
                 */
                host: 'http://your-sitecore-installation.local',


                /**
                 * The themePath is where in the Media Library we can find the theme you want to work on. The prefix
                 * of `sitecore/Media Library/` is assumed, so a path like this:
                 *
                 * `/sitecore/media library/Themes/B2B/B2B Shared Site/B2B Theme`
                 *
                 * should be entered like this:
                 *
                 * `/Themes/B2B/B2B Shared Site/B2B Theme`
                 */
                themePath: "/Themes/B2B/B2B Shared Site/B2B Theme",


                /**
                 * You will always be asked about your login when you start synchronizing. These will be the default
                 * values for the login prompt, and you can change them to what your admin-user is configured to, but
                 * FOR THE LOVE OF GOD DO NOT CHECK IN THE CHANGE!!!
                 *
                 * I REPEAT: IF YOU CHANGE THESE DEFAULTS, DO NOT CHECK THEM IN!
                 *
                 * If I find a repository with your administrator password entered in cleartext, I will find you and
                 * set you on fire. Slowly.
                 */
                login: {
                    defaultUsername: "sitecore\\admin",
                    defaultPassword: "b"
                },


                /**
                 * The remote-settings are used to determine which endpoints to talk to, and which databases to use,
                 * and all that jazz. You probably won't need to change these things.
                 */
                remoteSettings: {
                    removeFileScriptPath: '/-/script/v2/master/RemoveMedia',
                    uploadFileScriptPath: '/sitecore modules/PowerShell/Services/RemoteScriptCall.ashx',
                    updateTemplateScriptPath: '/-/script/v2/master/ChangeTemplate',
                    scDatabase: "master",
                    scriptDB: "master"
                },
            },

        },



        svg: {
            /**
             * Outputting individual SVG-files and injecting them in the DOM at build- or runtime is very much preferred.
             * Here's how to import an SVG-file across various platforms:
             *
             * Cross-platform C# CMS (confirmed on SiteCore, Umbraco, EpiServer):
             *     @Html.Raw(File.ReadAllText(Server.MapPath("~/static/dist/svg/name-of-svg.svg")))
             *
             * Umbraco (newer baseline):
             *     @Static.IncludeFile("~/static/dist/svg/name-of-svg.svg")
             *
             * SHTML:
             *     <!--#include virtual="/static/dist/svg/name-of-svg.svg" -->
             *
             * Javascript (note the source path!). Inject it using element.innerHTML, the "html" property in createElement(), or dangerouslySetInnerHTML in React.
             *     import someIcon from '../svg-individual/name-of-svg.svg';
             *     window.console.log(someIcon);
             *
             *
             * */
            individualFiles: {
                enable: true,
                sourceFolder: "svg-individual",
                targetFolder: "svg",
                removeIDsFromOutput: true
            },

            /**
             * This subtask builds a bundle of all your SVG-files and lets you refer to the content using <svg><use>,
             * but though this cuts the amount of requests down drastically, compared to inserting links to individual
             * files, there are drawbacks to this technique. Please try to have the CMS inject the SVG content, because
             * then there are NO javascript-dependencies (svg4everybody) and no extra requests – plus, it lets you animate
             * and style the individual parts of the SVG, so everybody wins.
             *
             * What's that, you say? You're making a static HTML site and have no CMS? Just use the SHTML-capabilities,
             * and then render to HTML from there. This can be fully automated in minutes... just ask!
             *
             * Ideally, you'd not use this part at all.
             */
            bundle: {
                enable: true,
                sourceFolder: "svg-bundle",
                targetFolder: "svg",
                bundleName: "_bundle.svg",
                /** Cheerio is used to clean up the SVGs before bundling them. */
                cheerio: {
                    removeDefs: true
                },
                removeIDsFromOutput: true
            },



            cleanTargetFolders: [
                "svg"
            ],
            watch: true,
            watchFor: "*.svg",
            watchFolders: [
                "svg-bundle",
                "svg-individual"
            ]
        },




        /**
         * Documentation makes everything easier for us and the robots that help us.
         * This task is for generating an easy-to-read set of HTML-files with descriptions and examples
         * for all our utilities. - Good for beginners and for brushing up.
         */
        docs: {
            sources: {
                readme: "README.md",
                changelog: "CHANGELOG.md",
                scss: [
                    "scss/utilities",
                    "scss/layout"
                ],
                js: [
                    "js/utils"
                ]
            },
            targetFolder: "docs",

            /**
             * Globals are utilities without the @module directive
             * We don't like those, and listing them as incomplete is helpful when working on new utilities.
             */
            listGlobalsAsIncomplete: true
        }
    }
};

module.exports = configuration;
