# Changelog
All notable changes to **Frontline** will be documented in this file.  
Versioning adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).




***

## [3.12.0](#markdown-header-3120-2019-04-23) 2019-04-23

#### Added
- The utility `utils/events/gesture` for handling mouse and touch gestures.
- Another utility `utils/interaction/notice` for showing notifications, like announcements or warnings, on a page.
- `getPointBetween` added to `utils/calc/coordinates`. For getting the coordinates of a point between two other endpoints.
- In `utils/dom/elementProperties`, the functions `getElementSize`, `getElementWidth` and `getElementHeight` now have a new option for including scale set by the `transform` property in CSS.


#### Changed
- `utils/timer` now has a `destroy` method.
- The docs now include a page for the changelog and menu items for accessing the changelog and front page.
- Updated dependencies to newer versions.




## [3.11.1](#markdown-header-3111-2019-04-11) 2019-04-11

#### Changed
- Stylelint now includes rules for `selector-combinator-space-before` and `selector-combinator-space-after`, throwing errors if CSS combinators (>, + and ~) aren't surrounded by spaces.




## [3.11.0](#markdown-header-3110-2019-04-05) 2019-04-05

#### Added
- The utility `utils/dom/scrollPosition` for calculating the position of the current scroll state on a page.




## [3.10.0](#markdown-header-3100-2019-03-25) 2019-03-25

#### Added
- A brand new lazy-loader `utils/dom/lazyLoad` with support for many more types of elements, image placeholders for avoiding reflow, and asynchronous loading and decoding of images.
- Two new functions for matching tag names up against elements, `isElementTag` and `areAllElementsTag`, in the utility `utils/dom/elementProperties`.
- The eslint plugin `indent-class-properties` for a rule on indentation on class properties.
- The `lint-staged` NPM dependency.


#### Changed
- By using `lint-staged` for git hooks we only run tests on staged files, making it easier to commit and push while still working on other files.
- Added support for `HTMLCollection` in `forEach` utility.
- Fixed a bug causing initial build to fail by catching errors in `gulp/tasks/javascript.js` when failing to fetch the polyfill fallback.
- Updated `utils/network/loadImage` to support srcset and reject its promise, if the image can't be loaded.
- Fixed a small miscalculation and added support for the x-axis in `isElementInViewport`.


#### Removed
- The `husky` NPM dependency. This is a dependency of the newly added `lint-staged`, so we don't need both.




## [3.9.1](#markdown-header-391-2019-03-12) 2019-03-12

#### Changed
- The class name that is set on the html tag by `utils/dom/scrollLock` has been changed to `akqa-scroll-lock`.
- The "link" tag for the main javascript file `main.min.js` in the head of `index.shtml` is now set to preload instead of prefetch.




## [3.9.0](#markdown-header-390-2019-02-15) 2019-02-15

#### Added
- A `stylesRendered` callback handler for detecting when styles are lazy-loaded and repainted.
- A new utility `isElementInside` has been added to `utils/dom/elementProperties`.
- The `sxa` task, which will let you synchronize the output from Frontline with the Sitecore SXA media library.


#### Changed
- `utils/dom/lazyLoader` and `utils/dom/imageHandler` now use URI encoding to keep characters like whitespaces from messing up the paths to the images.
- Fixed some bugs in `getElementPosition` in `utils/dom/elementProperties`.
- The JS- and SCSS-tasks have been updated to be more configurable with regards to where their final output data ends up and what it'll be called.
- Renamed the remaining custom events containing "dis" to something in the vein of `akqa.moduleName:eventName`.
- Major cleanup including:
    - A lot of reformatting. Mostly uniforming the use of spaces, line breaks, quotation marks and trailing commas.
    - Annotated old classList functions as deprecated.
    - A handful of functions and variables have been renamed, so booleans start with something like `is` or `has`, and getters start with `get`.
    - Some errors in examples have been fixed.




## [3.8.5](#markdown-header-385-2019-02-05) 2019-02-05

#### Changed
- `Buffer()` is deprecated due to security and usability issues. `gulp/helpers/groupCSSMediaQueries.js` now uses `Buffer.from()`.
- In `parseHTML()` in `utils/dom/elementManipulation`, an array with a spread opearator has been changed to `Array.from` to support Edge 15.
- Stylelint updated. Removed duplicated ignores and decreased the amount of capture groups in `selector-class-pattern`.




## [3.8.4](#markdown-header-384-2019-01-29) 2019-01-29

#### Changed
- A new rule has been added to stylelint to make sure BEM syntax is used correctly.
- Stylelint now accepts `:local` and `:global` which is used by CSS-modules for CSS in JS.
- The properties in `.eslintrc.json` and `.stylelintrc.json` have been sorted alphabetically to make them more manageable.
- The CSS properties in the SCSS utilities have also been sorted alphabetically.




## [3.8.3](#markdown-header-383-2019-01-28) 2019-01-28

#### Changed
- Bug fixed in `utils/events/onScrollThreshold`. Adding new callbacks to an element removed old callbacks.
- The SCSS mixin `prepend-selector` has been updated to support pseudo classes and pseudo elements.




## [3.8.2](#markdown-header-382-2018-12-18) 2018-12-18

#### Changed
- The Stylelint property `value-list-comma-newline-after` has been changed from `always` to `always-single-line`, now allowing CSS property values to be broken into multiple lines.




## [3.8.1](#markdown-header-381-2018-11-27) 2018-11-27

#### Added
- A local version of `gulp-cssnano`, since the official package is not maintained anymore, and the reliance on a forked version on Github can lead to unstable behavior on some build systems. This adds the following NPM dependencies: `browserslist`, `buffer-from`, `cssnano`, `object-assign`.
- Minor documentation additions.


#### Changed
- `onlineTracker` moved into `network` folder, and changed to not use `dis-` class prefixes anymore.


#### Removed
- `gulp-cssnano` NPM dependency.




## [3.8.0](#markdown-header-380-2018-11-06) 2018-11-06


#### Added
- The `javascript:polyfills` task, which locally caches a snapshot of the polyfills needed for the site to be used as a fallback if polyfill.io goes down.
- A React bootstrapper with support for asynchronous/lazy module loading, much like the regular `asyncBootstrapper`. It lives in `utils/react/reactBootstrapper`.
- The “glob” dependency, used for the javascript task.
- A `prefetchRequest` utility to easily prefetch heavy resources programmatically before they are needed. It lives in `utils/network/prefetchRequest`.
- An autosuggest class for easily handling stuff like typeahead during searches. Lives in `utils/interaction/autoSuggest.js`.
- A ColorPicker class in `utils/interaction/colorPicker` for creating highly customizable color pickers.
- Utilities for manipulating colors in `utils/calc/colors/colorManipulation`:
    - `mixColors` for mixing two colors the same way SASS does it (with a weight parameter).
    - `multiplyColors` for mixing two colors with the multiply blend mode.
    - `screenColors` for mixing two colors with the screen blend mode.


#### Changed
- The polyfill loader in the HTML has been augmented with a fallback handler in case the main polyfill server goes down.
- Service worker task now actually works again.
- SVG “individual” task will now never remove the `viewBox` attribute from processed files.
- `loadImage` utility moved from `utils/dom` into `utils/network`, since it deals exclusively with prefetching and decoding images and has no DOM-specific functionality at all.
- Folder watches in `watch` task can now work with negation patterns in order to exclude subfolders from file-watching. This is employed in the SCSS-task already.
- JS-task now supports combining multiple entrypoints into a single payload by using object notation instead of arrays.
- Updates to `FormValidate`:
    - Class names renamed to use BEM syntax.
    - Class names moved into the settings object, so they can be changed to fit the developer’s needs.
    - Fire callback `beforeSubmit` before validating the field when submitting a form.
    - Validation changed to always use the polyfill for `validityCheck`, since the native API in the browsers is too unreliable.
    - Bug fixed so we now support fields with squared brackets and dots in their IDs.




## [3.7.1](#markdown-header-371-2018-10-29) 2018-10-29


#### Changed
- The indent rule in `.eslintrc.json` has been updated to enforce indentation in switch cases.
- Indentation updated in `utils/dom/createElement` and `utils/events/keyEvents` to follow the new rule.




## [3.7.0](#markdown-header-370-2018-10-23) 2018-10-23


#### Added
- A Google Maps helper utility file to make using Google Maps easier. This adds `loadMapsAPI()`, `getLocationFromAddress()`, `getAddressFromLocation()` and `getAddressFromPlaceId()`, as well as supporting helper functions.
- `getBounds()`utility for getting the x and y coordinates of the boundaries from a set of coordinates.
- The class `ColorMatch` for creating a palette of accepted colors, and finding the closest or farthest matches to any other color in this palette.
- Utilities for converting between different types of color values, such as RGB, HSL, hex and color names.
- Color combination utilities for finding different types of color schemes (analogous, complementary, split complementary, triadic and tetradic).
- `isArray()` utility inside new `utils/typeCheckers` file for easily determining whether or not something is an array.
- `setAttributes()` and `removeAttributes()` utilities added to `utils/dom/elementProperties` for easily, and more intelligently, dealing with element attributes.


#### Changed
- `utils/dom/isElement` is deprecated, and the functionality has been moved to `utils/typeCheckers` instead.
- `utils/dom/createElement()` utility cleaned up a bit based on the newly added attribute handlers.
- Modal renderer now uses `createElement()` much more in the initial rendering in order to combat various DOM-timing-related issues when parsing strings to HTML.
- The docs created by running `npm run docs` now adds links to type definitions from the parameter types even if there are multiple types in one.


#### Removed
- `msbuild`-task and related dependencies.




## [3.6.11](#markdown-header-3611-2018-10-22) 2018-10-22


#### Changed
- `cssnano` did not handle autoprefixing the way we thought it did. The `scss` gulp task has been updated to autoprefix correctly.




## [3.6.10](#markdown-header-3610-2018-10-16) 2018-10-16


#### Changed
- The `Modal` DOM-builder no longer uses `appendElement`, since apparently that performs too many calls for the thread to want to wait for the DOM to finish. This entire renderer is due for a rewrite, but for now, this bugfix keeps it afloat.




## [3.6.9](#markdown-header-369-2018-10-09) 2018-10-09


#### Changed
- The Frontline repository has been renamed, and some local checks have been updated accordingly.




## [3.6.8](#markdown-header-368-2018-10-08) 2018-10-08


#### Added
- A checksum utility, with a corresponding checksum file that keeps track of internal Frontline utility files. If you want to upgrade parts of Frontline, you can run `npm run preupgrade` to see if any files have been altered since their installation. Keep in mind this might not be a fool-proof system, but it's better than nothing.


#### Changed
- Performance on DOM manipulations based on strings has been massively improved.
- `forEach` utility is now much faster when working on pure arrays.
- The SVG task has received an overhaul, and now supports concurrency better and will only render working-messages while actually working.
- Type definitions `PasswordOptions` and `InViewElement` corrected.
- Lots of improvements in documenting supported parameter types for various functions.
- Javascript task augmented with better configuration control and prepared for fine-grained code splitting.




## [3.6.7](#markdown-header-367-2018-09-28) 2018-09-28


#### Added
- Two new functions in the utility `js/utils/calc/coordinates`:
    - `getAllDistances()` for getting all distances between multiple coordinates, sorted from shortest to longest.
    - `getAngle()` for getting the angle from one set of coordinates to another.


#### Changed
-  More changes to`js/utils/calc/coordinates`:
    - In addition to arrays, `getDistance()` and `getSlope()` now take objects with coordinates as well.
    - Added type definitions for defining these objects with coordinates `{x: Number, y: Number}`
- The media query combine/sort-helper for the SCSS compilers has been updated considerably. It now handles `min-height` and `max-height`, whereas it didn't before. It also works with combinations of size-definitions, but please exercise caution when building multilayered media queries. No action is needed from you to take advantage of this update.




## [3.6.6](#markdown-header-366-2018-09-27) 2018-09-27


#### Added
- New utility `js/utils/interaction/modal`. This can be used to open fully configurable modal dialogs, or the built-in shortcuts can be used for regular `alert`/`confirm`-style interactions.
- Added the function `getFirstInteractiveElementInContainer()` to `js/utils/dom/accessibility.js`. This gets the first interactive element in a container for you.


#### Changed
- Changes to `js/utils/elementManipulation.js`:
    - Bugfix: `parseHTML()` has been updated with much better handling of HTML-strings, and now returns an array of DOM-nodes. The utilities depending on it have been updated accordingly.
- Updated documentation all over.




## [3.6.5](#markdown-header-365-2018-09-26) 2018-09-26


#### Added
- The `webpack-bundle-analyzer` dependency.
- A javascript bundle size analyzer. It can be started by running `npm run sourcemap` in the terminal, and lets you find out what, if anything, is bloating your code.
- The `stretchColumn()` SCSS mixin, used for stretching grid columns beyond their regular borders when needed.
- Added the function `parseHTML()` to `js/utils/dom/elementManipulation.js`. This parses a string into a DOM node and returns said node.
- New utility `js/utils/events/onScrollThresholds`. This is useful for hiding and showing elements on scroll.


#### Changed
- Changes to `js/utils/formValidate.js`:
    - Bugfix: A default value for `options` was missing from the constructor. 
    - The variable `this.elementsToValidate` has been changed from a single string with a selector to an array of strings with tag names.
- Changes to `js/utils/dom/elementManipulation.js`:
    - Instead of inserting one element after another, elements are now placed in a fragment created by `createDocumentFragment()`. This way the DOM only recalculates and repaints _once_ instead of once per element.
    - `appendElement`, `prependElement`, `insertElementAfter` and `insertElementBefore` now has support for inserting strings.




## [3.6.4](#markdown-header-364-2018-09-24) 2018-09-24


#### Changed
- `js/utils/dom/elementProperties.js` has been updated with the following methods: `elementInViewport()`, `getElementPosition()`, `getElementScroll()`, `getElementSize()`, `getElementWidth()`, and `getElementHeight()`. This is in order to keep the amount of separate `Element` utility files from exploding as we add more and more features. All existing imports have been updated accordingly. 
- Upgraded versions of all devDependencies, where applicable.


#### Removed
- Following the consolidation of various element property readers, the following files have been removed:
    - `js/utils/dom/elementInViewport.js`
    - `js/utils/dom/getElementPosition.js`
    - `js/utils/dom/getElementScroll.js`
    - `js/utils/dom/getElementSize.js`




## [3.6.3](#markdown-header-363-2018-09-20) 2018-09-20


#### Added
- The `ssi` dependency.
- FormValidate has been moved from the modules branch to master. It has also been improved with a few fixes to its type definitions and better polyfilling.
- FormValidate now saves the filled out values in locale storage if the user is offline.


#### Changed
- The use of the dependency `node-ssi` has been replaced with `ssi` in `gulp/tasks/helpers/browsersync-ssi.js`, since the former can't handle relative paths containing `../`.
- Bugfixes for the documentation generated by `npm run docs`
    - Parts of SCSS descriptions were parsed as code-blocks due to indentation.
    - Code blocks of pure SCSS generated by SassDoc had wrong indentation.
- Adjusted some comments for SCSS mixins and `grid.scss`.
- `js/utils/dom/parents.js` now uses `.contains()` to find child/parent relations.


#### Removed
- The `node-ssi` dependency.




## [3.6.2](#markdown-header-362-2018-09-14) 2018-09-14


#### Changed
- `js/utils/dom/tabDomElements.js` has been updated. You can now give it an array of elements to exclude.




## [3.6.1](#markdown-header-361-2018-09-14) 2018-09-14


#### Added
- Some customized SSI middleware for BrowserSync at `gulp/tasks/helpers/browsersync-ssi.js`.
- A new depencency `node-ssi`. 
- A `CHANGELOG.md` file.


#### Changed
- `gulp/tasks/helpers/browsersync.js` is using our own `browsersync-ssi.js` instead of the dependency `browsersync-ssi`.
- From now on, we'll bump the version number (adhering to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)) in `package.json` every time there are any changes.


#### Removed
- The `browsersync-ssi` dependency.
