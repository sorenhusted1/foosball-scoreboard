const vfs = require("vinyl-fs");
const map = require("map-stream");


/**
 * Find out how many files would be returned for a given Vinyl folder/file glob pattern.
 * Returned as a promise to allow asynchronous operation.
 *
 * @param {string} folderAndFilenameGlob - The file glob to test. Ideally you'd set this to something like "some/folder/*.txt"
 * @returns {Promise<Number>} A promise that resolves with the number of files matching the pattern.
 */
const countFilesInDirectory = folderAndFilenameGlob =>
    new Promise(reportFileCount => {

        let count = 0;

        vfs.src(folderAndFilenameGlob, { read: false})
            .pipe(map((file, cb) => {
                if (!file.isDirectory()) {
                    count += 1;
                }
                cb(null, file);
            }))
            .on("end", () => reportFileCount(count))
        ;
    });


module.exports = countFilesInDirectory;
