/*global __dirname, require */
const colors = require("ansi-colors");
const config = require('../../setup/config');
const log = require("fancy-log");
const packageInfo = require('../../package');
const map = require('map-stream');
const vfs = require('vinyl-fs');
const fs = require('fs');
const crypto = require('crypto');
const path = require('path');
const args = require('yargs').argv;
const checksumFilename = path.resolve(__dirname, '../checksum.json');

const baseFolder = path.resolve(config.folders.root);
const gulpFolder = path.resolve(config.folders.root, "gulp/");
const codeFolder = path.resolve(config.folders.root, config.folders.sourceFolder);

const generatedChecksums = {
    package: `${packageInfo.name} ${packageInfo.version}`,
    files: {}
};


// These are the files we'll keep track of.
const filesToCheck = [
    `${gulpFolder}/taskLoader.js`,
    `${gulpFolder}/tasks/**/*.js`,
    `${gulpFolder}/helpers/**/*.js`,
    `${codeFolder}/js/utils/**/*.js`,
    `${codeFolder}/scss/utilities/**/*.scss`,
    `${codeFolder}/scss/layout/grid.scss`,
];



const generateChecksum = str => crypto
    .createHash('md5')
    .update(str, 'utf8')
    .digest('hex');



const generateChecksumForFile = (file, cb) => {
    const relativeFilePath = path.relative(baseFolder, file.path);

    fs.readFile(file.path, (err, data) => {
        const checksum = generateChecksum(data);
        generatedChecksums.files[relativeFilePath] = checksum;
        cb(null, file);
    });
};



const handleResponse = () => {

    // Store generated checksums.
    if (args.generate) {

        // Make sure we only do this for the "live" Frontline repository! Anything else would be really dumb. Fo' real.
        new Promise((everythingIsAwesome, everythingIsHorrible) => {

            // "child_process" is a native Node module. Don't worry about it.
            // https://nodejs.org/docs/latest-v8.x/api/child_process.html
            const { spawn } = require('child_process');

            // Run the command "git remote get-url origin".
            const gitCheck = spawn('git', ['remote', 'get-url', 'origin']);

            // We got data back! Hopefully this is the most regular use case. The data is a buffer that should contain
            // the name of the upstream repository... if that matches Frontline v3, we know the user is working on
            // Frontline itself, and they shouldn't be warned. If not, they're working on another repository, and that
            // is a problem.
            gitCheck.stdout.on('data', data => {
                if (data.toString().indexOf("displaydk/frontline") > -1) {
                    everythingIsAwesome();
                } else {
                    everythingIsHorrible();
                }
            });

            // If a non-fatal error is returned, we'll take it as a problem. It's probably because there isn't an
            // active git repo here, so it isn't Frontline.
            gitCheck.stderr.on('data', () => everythingIsHorrible);

            // If the process crashes entirely, it's possible git isn't even installed. This is also a problem.
            gitCheck.on('error', () => everythingIsHorrible);

        })
            .then(
                () => fs.writeFile(
                    checksumFilename,
                    JSON.stringify(generatedChecksums),
                    () => log(colors.green(`Checksums generated/updated for ${colors.yellowBright(colors.bold(Object.keys(generatedChecksums.files).length))} files.`))
                )
            )
            .catch(
                () => log(colors.red("You can only generate and store checksums for the Frontline repo itself!"))
            );

    }



    // Check files against existing checksum. This is the most normal use-case.
    if (args.check) {
        let missingFiles = 0,
            errors = 0;

        fs.readFile(checksumFilename, (err, data) => {
            const originalData = JSON.parse(data);
            Object.keys(originalData.files).forEach(key => {
                const currentChecksum = originalData.files[key];
                if (generatedChecksums.files[key]) {
                    if (generatedChecksums.files[key] !== currentChecksum) {
                        log(colors.red(`* ${key} has been ${colors.underline(colors.red('changed'))} compared to ${colors.bold(colors.redBright(originalData.package))}!`));
                        errors += 1;
                    }
                } else {
                    log(colors.yellow(`* ${key} no longer exists, but it did in ${originalData.package}.`));
                    missingFiles += 1;
                }
            });

            console.log("");
            if (errors || missingFiles) {
                console.log("");
                log(colors[errors ? 'red' : 'yellow']('********************************************************************************************************************'));
                if (errors) log(colors.red(`Found ${errors} potential dangerous alteration${errors === 1 ? '' : 's'} you need to check before upgrading!`));
                if (missingFiles) log(colors.yellow(`${missingFiles} file${missingFiles === 1 ? '' : 's'} ha${missingFiles === 1 ? 's' : 've'} been removed from the local codebase compared to ${originalData.package}. This is probably not a problem for you.`));
            } else {
                log(colors.green(`No alterations spotted in ${Object.keys(generatedChecksums.files).length} tracked core files from ${originalData.package}. You are free to upgrade!`));
            }
        });
    }
};


// Generate the checksums first, no matter what.
vfs.src(filesToCheck)
    // Do the calculation thingie
    .pipe(map(generateChecksumForFile))
    // Find out what to do with the data.
    .on("end", handleResponse);

