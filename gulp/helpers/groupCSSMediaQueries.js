/**
 * This file is a "hacked" version, combining:
 *  - gulp-group-css-media-queries ( http://npmjs.com/package/gulp-group-css-media-queries )
 *  - group-css-media-queries ( https://www.npmjs.com/package/group-css-media-queries )
 *  - group-css-media-queries PR #16 ( https://github.com/SE7ENSKY/group-css-media-queries/pull/16 )
 *
 * A clean NPM package would've been nicer, but since development on the original package seems to have
 * stalled somewhat, we're gonna build our own utility instead. With black jack! And hookers!
 */


const through = require('through2');
const parseCss = require('css-parse');
const stringifyCss = require('css-stringify');
const emToPxRatio = 16;


/**
 *
 * @private
 * @param {string} media - Media string, ie. "(min-width: 250px)"
 * @param {object} mediaRule - Existing media rule object to put the data into.
 * @param {string} lookFor - The media query we're looking for right now (ie. "min-width")
 * @returns {Object}
 */
function parseMediaQueryRule(media, mediaRule, lookFor) {
    if (media.indexOf(lookFor) > -1) {
        // Set up a regexp to match our data against.
        const match = new RegExp(`${lookFor}:\\s*(\\d+(?:\\.\\d*)?)(px|r?em)?`);

        // Do the match thing!
        const mediaMatch = match.exec(media);

        // If we have match results...
        if (mediaMatch) {

            // Parse the rule-name (ie. "min-width"), and translate it to camelCase (ie. "minWidth").
            const ruleNameMatch = lookFor.match(/^([a-z]*)-([a-z])([a-z]*)$/i);
            const namedRule = `${ruleNameMatch[1]}${ruleNameMatch[2].toUpperCase()}${ruleNameMatch[3]}`;

            // Get the value, ie. "550px" and parse it to a floating point number.
            if (mediaMatch[1]) {
                mediaRule[namedRule] = parseFloat(mediaMatch[1]);
            }

            // Get the unit and store it.
            if (mediaMatch[2]) {
                mediaRule[`${namedRule}Unit`] = mediaMatch[2];
            }
        }
    }

    return mediaRule;
}


/**
 * Get a sorter tied to the given rule name, ie. "minWidth".
 *
 * @private
 * @param {string} ruleName
 * @param {string} alternateRuleName
 * @returns {Function}
 */
const getRuleSorter = (ruleName, alternateRuleName) => (a, b) => {
    let aPxValue = a[ruleName] || a[alternateRuleName];
    let bPxValue = b[ruleName] || b[alternateRuleName];
    const ruleNameUnit = `${ruleName}Unit`;

    if (aPxValue !== undefined && bPxValue !== undefined) {

        if (a[ruleNameUnit] === 'em' || a[ruleNameUnit] === 'rem') {
            aPxValue *= emToPxRatio;
        }
        if (b[ruleNameUnit] === 'em' || b[ruleNameUnit] === 'rem') {
            bPxValue *= emToPxRatio;
        }
        return aPxValue - bPxValue;
    }
};




function groupMediaQueries(css) {
    const parsed = parseCss(css);
    const medias = {};
    const rootRules = [];
    const mediaRules = [];

    // Parse the stylesheet and get the rule arrays we need.
    parsed.stylesheet.rules.forEach(rule => {
        if (rule.type === 'media') {
            if (!medias[rule.media]) {
                medias[rule.media] = [];
            }
            medias[rule.media] = medias[rule.media].concat(rule.rules);
        } else {
            rootRules.push(rule);
        }
    });

    // Parse the rules to get our various breakpoint values as integers so we can sort them later.
    Object.keys(medias).forEach(media => {

        const rules = medias[media];
        let rule = {
            type: "media",
            media,
            rules
        };

        rule = parseMediaQueryRule(media, rule, "min-width");
        rule = parseMediaQueryRule(media, rule, "max-width");
        rule = parseMediaQueryRule(media, rule, "min-height");
        rule = parseMediaQueryRule(media, rule, "max-height");

        mediaRules.push(rule);

    });

    // Get all rules that are only "min-width" and/or "min-height", and nothing else.
    const onlyMinRules = mediaRules.filter(rule =>
        (rule.minWidth !== undefined || rule.minHeight !== undefined) &&
        (rule.maxWidth === undefined && rule.maxHeight === undefined)
    );

    // Get all rules that are only "max-width" and/or "max-height", and nothing else.
    const onlyMaxRules = mediaRules.filter(rule =>
        (rule.maxWidth !== undefined || rule.maxHeight !== undefined) &&
        (rule.minWidth === undefined && rule.minHeight === undefined)
    );

    // Get all rules that contain intervals between min-width and max-width, or between
    // min-height and max-height.
    const intervalRules = mediaRules.filter(rule =>
        rule.minWidth ? (rule.maxWidth) :
            rule.minHeight ? (rule.maxHeight) :
                false
    );

    // Get all rules that weren't included in the other sets.
    const otherRules = mediaRules.filter(rule => [].indexOf.call(
        onlyMinRules
            .concat(onlyMaxRules)
            .concat(intervalRules),
        rule) < 0);


    // Sort the various min- and max-rules.
    onlyMinRules.sort(getRuleSorter('minWidth', 'minHeight'));
    onlyMinRules.sort(getRuleSorter('minHeight', 'minWidth'));
    onlyMaxRules.sort(getRuleSorter('maxWidth', 'maxHeight'));
    onlyMaxRules.sort(getRuleSorter('maxHeight', 'maxWidth'));

    // Sort the intervals by anything we can think of.
    intervalRules.sort(getRuleSorter('minWidth', 'minHeight'));
    intervalRules.sort(getRuleSorter('minHeight', 'minWidth'));
    intervalRules.sort(getRuleSorter('maxWidth', 'maxHeight'));
    intervalRules.sort(getRuleSorter('maxHeight', 'maxWidth'));


    parsed.stylesheet.rules = rootRules
        .concat(onlyMinRules)
        .concat(onlyMaxRules)
        .concat(intervalRules)
        .concat(otherRules);


    return stringifyCss(parsed);
}


module.exports = function () {
    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        try {
            file.contents = Buffer.from(groupMediaQueries(file.contents.toString()));
        } catch (err) {
            this.emit('error', err.message);
        }

        this.push(file);
        cb();
    });
};

