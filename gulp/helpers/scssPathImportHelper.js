const { config } = global.akqa;
const path = require('path');
const fs = require('fs');


/**
 * An import path helper for SCSS importing. Can resolve the resource root or `node_modules` by the use of pure magic.
 *
 * @author Anders Gissel <anders.gissel@akqa.com>
 * @param {string} importURL
 * @param {string} currentFile
 * @returns {{file: string}|{contents: string}|null}
 */
const scssPathImportHelper = (importURL, currentFile) => {

    const resolvedTarget = (importURL.substr(0, 2) === '~/') ?
        // If the URL begins with '~/', we'll interpret it as a reference to the source root folder (ie. "static/src", or wherever)
        path.resolve(config.folders.root, config.folders.sourceFolder, importURL.substr(2)) :

        // If the URL just begins with '~', we'll interpret it as a reference to 'node_modules' instead. If not, return a null
        // (telling the compiler we won't do anything about this).
        (importURL[0] === '~') ? path.resolve(config.folders.root, "node_modules", importURL.substr(1)) : null;

    if (resolvedTarget) {

        // If this is a CSS-file, inline it immediately!
        if (path.extname(resolvedTarget).toLowerCase() === ".css") {
            const contents = fs.readFileSync(resolvedTarget, 'utf8');
            return { contents };
        } else {
            // Everything else just gets treated normally.
            const baseDir = path.dirname(currentFile);
            const relativePath = path.relative(baseDir, resolvedTarget);
            return { file: relativePath };
        }

    }

    return null;
};

module.exports = scssPathImportHelper;
