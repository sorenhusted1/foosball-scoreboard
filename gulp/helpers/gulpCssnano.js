/*global setImmediate */

const nano      = require('cssnano'),
    bufferFrom  = require('buffer-from'),
    assign      = require('object-assign'),
    PluginError = require('plugin-error'),
    Transform   = require('stream').Transform,
    applySourceMap = require('vinyl-sourcemaps-apply'),

    PLUGIN_NAME = 'gulp-cssnano';

module.exports = function (options) {
    const opts = options || {};
    const stream = new Transform({objectMode: true});

    stream._transform = function (file, encoding, cb) {
        if (file.isNull()) {
            return cb(null, file);
        }
        if (file.isStream()) {
            const error = 'Streaming not supported';
            return cb(new PluginError(PLUGIN_NAME, error));
        } else if (file.isBuffer()) {
            return nano.process(String(file.contents), assign(opts, {
                map: (file.sourceMap) ? {annotation: false} : false,
                from: file.relative,
                to: file.relative
            })).then(result => {
                if (result.map && file.sourceMap) {
                    applySourceMap(file, String(result.map));
                }
                file.contents = bufferFrom(result.css);
                this.push(file);
                cb();
            })
                .catch(error => {
                    const errorOptions = {fileName: file.path};
                    let displayError;
                    if (error.name === 'CssSyntaxError') {
                        displayError = error.message + error.showSourceCode();
                        errorOptions.showStack = false;
                    } else {
                        displayError = error.toString();
                    }

                    // Prevent stream’s unhandled exception from
                    // being suppressed by Promise
                    setImmediate(() => cb(new PluginError(PLUGIN_NAME, displayError)));
                });
        }
    };

    return stream;
};
