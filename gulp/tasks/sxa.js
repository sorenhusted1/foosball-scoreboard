/*global process */
const { gulp, config } = global.akqa;
const colors = require("ansi-colors");
const fancyLog = require("fancy-log");
const path = require("path");
const fs = require("fs");
const fetch = require("node-fetch");

// Placeholders for later.
let loginPromise, currentCredentials, actionQueue;

const LOG_SIGNATURE = "[SXA]";

const PAYLOAD_TYPES = {
    CREATE: "created",
    CHANGE: "changed",
    DELETE: "deleted"
};


// Find out where our destination folder is. If there are more than one, we'll use the first one we can find.
const DESTINATION_FOLDER = path.resolve(config.folders.root, config.folders.targetFolder.constructor === Array ? config.folders.targetFolder[0] : config.folders.targetFolder);



let payload = {};


let debouncedPayloadPurge;
const schedulePayloadSend = () => {
    if (!debouncedPayloadPurge) {
        const debounce = require("debounce");
        debouncedPayloadPurge = debounce(helpers.sendPayloads, config.tasks.sxa.actionDebounceDelay);
    }
    debouncedPayloadPurge();
};




const log = message => fancyLog(`${colors.greenBright(colors.bold(LOG_SIGNATURE))} ${message}`);



const helpers = {

    /**
     * Queue a function to be executed once there's room for it. Bear in mind that the queue will
     * not be started automatically.
     *
     * @param {function} functionReference
     */
    queue: functionReference => {
        if (!actionQueue) {
            const queue = require("queue");
            actionQueue = queue();
        }
        actionQueue.push(functionReference);
    },


    /**
     * We like us some generic error messages, don't we?
     *
     * @param {string} [message=""] - Additional log message for the user.
     */
    throwError: (message = "") => {
        log(colors.red(`Something went wrong! Make sure you have enabled Gulp-integration by renaming the file `) + colors.yellowBright(`App_Config\\Include\\Feature\\z.SPE.Sync.Enabler.Gulp.config.disabled`) + colors.red(" to ") + colors.yellowBright("z.SPE.Sync.Enabler.Gulp.config"));
        log(colors.red("If you've already done that, make sure the login information is correct. You should enter credentials with administrative rights."));

        if (message) {
            log(colors.red("Further information: ") + colors.yellow(message.toString()));
            console.log("");
        }

        process.exit();
    },


    /**
     * Ask the user about their login information, and resolve the returned promise if they
     * fill it out.
     *
     * @returns {Promise}
     */
    login: () => {

        loginPromise = loginPromise || new Promise((resolve, reject) => {
            const inquirer = require("inquirer");

            log(`Prompting for login for Sitecore installation at ${colors.cyan(config.tasks.sxa.server.host)}`);
            inquirer.prompt([
                {
                    type: 'login',
                    name: 'login',
                    message: 'Enter your Sitecore-login',
                    default: config.tasks.sxa.server.login.defaultUsername
                },
                {
                    type: 'password',
                    name: 'password',
                    message: 'Enter your password',
                    default: config.tasks.sxa.server.login.defaultPassword
                }]
            ).then(answer => {
                if (!(answer.login.length && answer.password.length)) {
                    reject('loginError');
                    process.exit();
                } else {
                    currentCredentials = answer;
                    resolve(answer);
                }
            });
        });

        return loginPromise;
    },


    /**
     * Translate the filename to contain the extension as part of the "basic filename" if the
     * extension matches anything in the `transformFilenamesForExtensions` array. This is because
     * SiteCore is being silly.
     *
     * @param {string} filename
     * @returns {string}
     */
    translateFilename: filename => {

        const { dir, ext, name } = path.parse(filename);

        if (helpers.doesArrayOfPatternsMatchString(config.tasks.sxa.transformFilenamesForExtensions, ext)) {
            return path.normalize(`${dir}/${name}-${ext.replace(".", "")}${ext}`);
        }

        return filename;

    },


    /**
     * Synchronize a template with SXA by adding a fetch-job to the queue. The queue must
     * be emptied by running `helpers.sendPayloads()`)
     *
     * @param {string} filename - The name of the file containing the template.
     */
    doSyncTemplate: filename => {

        const { server } = config.tasks.sxa;

        const filePath = path.basename(filename);

        const url = `${server.host}${server.remoteSettings.updateTemplateScriptPath}?user=${encodeURIComponent(currentCredentials.login)}&password=${encodeURIComponent(currentCredentials.password)}&path=${encodeURIComponent(filePath)}`;

        const contentStream = fs.createReadStream(path.resolve(DESTINATION_FOLDER, filename));

        helpers.queue(() =>
            fetch(url, {method: 'POST', body: contentStream})
                .then(response => {

                    if (response.ok) {
                        log(`${colors.green("Template")} ${colors.cyan(colors.bold(filePath))} ${colors.green("has been synced on the server.")}`);
                    } else {
                        helpers.throwError();
                    }
                })
        );

    },


    /**
     * Upload a file to the SXA media library by adding a fetch-job to the queue. The queue
     * itself must be emptied by running `helpers.sendPayloads()`.
     *
     * @param {string} filename - The name of the resource file.
     */
    doUploadFile: filename => {

        if (helpers.checkIfSynchronizationIsAllowed(filename)) {

            if (helpers.checkIfFileIsATemplate(filename)) {

                helpers.doSyncTemplate(filename);

            } else {

                const filePathOnDisk = path.resolve(DESTINATION_FOLDER, filename);

                const fileDescriptor = fs.openSync(filePathOnDisk, 'r');
                const fileData = fs.fstatSync(fileDescriptor);
                fs.close(fileDescriptor);

                if (fileData.size > 0) {

                    const { server } = config.tasks.sxa;

                    const filePath = path.normalize(`${server.themePath}/${filename}`);

                    const translatedFilepath = helpers.translateFilename(filePath);

                    const url = `${server.host}${server.remoteSettings.uploadFileScriptPath}?user=${encodeURIComponent(currentCredentials.login)}&password=${encodeURIComponent(currentCredentials.password)}&script=${encodeURIComponent(translatedFilepath)}&sc_database=${encodeURIComponent(server.remoteSettings.scDatabase)}&apiVersion=media&scriptDb=${server.remoteSettings.scriptDB}`;

                    const contentStream = fs.createReadStream(filePathOnDisk);

                    helpers.queue(() =>
                        fetch(url, {method: 'POST', body: contentStream})
                            .then(response => {
                                if (response.ok) {
                                    log(`- ${colors.cyan(colors.bold(filename))} ${colors.green("has been synced to the server")}${translatedFilepath !== filePath ? ` ${colors.green("as")} ${colors.cyanBright(colors.bold(path.basename(translatedFilepath)))}` : ''}${colors.green(".")}`);
                                } else {
                                    helpers.throwError(`${response.status} ${response.statusText}`);
                                }
                            })
                            .catch(() => helpers.throwError())
                    );

                } else {
                    // Sitecore 9.01 will throw a 404 if you try to upload an empty file to it. Yeah. I know.
                    log(`${colors.red("Ignoring")} ${colors.cyan(filename)} ${colors.red("because it's a 0-byte size file.")}`);

                }

            }


        }

    },


    /**
     * Delete a file from the SXA media library by adding a fetch-job to the queue. The queue
     * must be emptied by throwing a handful of chicken bones in precisely the right pattern
     * during a full moon while chanting dark incantations, and also by running `helpers.sendPayloads()`.
     *
     * @param {string} filename - The name of the file to delete.
     */
    doDeleteFile: filename => {

        if (helpers.checkIfSynchronizationIsAllowed(filename, true)) {

            const {server} = config.tasks.sxa;

            const filePath = path.normalize(`${server.themePath}/${filename}`);
            const translatedFilepath = helpers.translateFilename(filePath);

            const url = `${server.host}${server.remoteSettings.removeFileScriptPath}?user=${encodeURIComponent(currentCredentials.login)}&password=${encodeURIComponent(currentCredentials.password)}&path=${encodeURIComponent(translatedFilepath)}&database=${encodeURIComponent(server.remoteSettings.scDatabase)}`;


            helpers.queue(
                () => fetch(url)
                    .then(response => response.json())
                    .then(response => {
                        if (!response.result && response.Reason) {
                            helpers.throwError(`${filename}: ${response.Reason}`);
                        } else {
                            log(`- ${colors.cyan(colors.bold(path.basename(translatedFilepath)))} ${colors.green("has been")} ${colors.red("deleted")} ${colors.green("from the server.")}`);
                        }
                    })
                    .catch(() => helpers.throwError())
            );
        }
    },


    /**
     * Check to see if an array of regexp-patterns contains at least one match for the given string.
     *
     * @param {RegExp[]} patterns - The array of patterns to check.
     * @param {string} input - The string to check against the patterns.
     * @returns {boolean} True if one of the patterns match on the string; false otherwise.
     */
    doesArrayOfPatternsMatchString: (patterns, input) => Boolean(patterns.find(pattern => pattern.test(input))),


    /**
     * Check if the filename is allowed on the server at all. We'll want to exclude some files from
     * the media library. You can define this in the configuration.
     *
     * @param {string} filename - The filename to check.
     * @param {boolean} [forDeleteOperation=false] - Delete operations should have a bit more restriction, so we'll match against additional patterns there.
     * @returns {boolean} True if the file should be allowed on the server; false otherwise.
     */
    checkIfSynchronizationIsAllowed: (filename, forDeleteOperation = false) => {

        let patternMatches = helpers.doesArrayOfPatternsMatchString(config.tasks.sxa.ignorePatternsForAllOperations, filename);

        if (!patternMatches && forDeleteOperation) {
            patternMatches = helpers.doesArrayOfPatternsMatchString(config.tasks.sxa.ignorePatternsForDeleteOperations, filename);
        }

        return !patternMatches;

    },


    /**
     * Check to see if the given filename is a template or not by matching it against a set of
     * template filename patterns. Templates will be treated differently on the server.
     *
     * @param {string} filename - The filename to check.
     * @returns {boolean} True if this file should be treated as a template; otherwise false.
     */
    checkIfFileIsATemplate: filename => helpers.doesArrayOfPatternsMatchString(config.tasks.sxa.templateFilePatterns, filename),


    /**
     * Send and then empty the queue of payloads. Will return a promise that resolves once all
     * the actions have completed, if you're into that sort of thing.
     *
     * @returns {Promise<*>}
     */
    sendPayloads: () =>
        new Promise(allIsWell => {

            const payloadFiles = Object.keys(payload);

            let uploadCount = 0,
                deleteCount = 0;

            payloadFiles.forEach(file => {
                if (payload[file] === PAYLOAD_TYPES.DELETE) {
                    deleteCount += 1;
                    helpers.doDeleteFile(file);
                } else {
                    uploadCount += 1;
                    helpers.doUploadFile(file);
                }
            });

            const totalCount = uploadCount + deleteCount;

            if (totalCount) {
                const logString = [];

                if (uploadCount) {
                    logString.push(`${colors.green("Uploading")} ${colors.yellow(colors.bold(uploadCount.toString()))} ${colors.green(`file${uploadCount !== 1 ? 's' : ''}`)}`);
                }

                if (deleteCount) {
                    logString.push(`${colors.red("Deleting")} ${colors.yellow(colors.bold(deleteCount.toString()))} ${colors.red(`file${deleteCount !== 1 ? 's' : ''}`)}`);
                }

                log(logString.join(" / "));

                actionQueue.start(() => {
                    log(`${colors.green(`${totalCount} file operation${totalCount !== 1 ? 's' : ''} complete.`)}`);
                    allIsWell();
                });

            } else {
                allIsWell();
            }

            payload = {};
        }),


    /**
     * Get a path/filename relative to the destination root folder.
     *
     * @param {string} file - The filename to resolve.
     * @returns {string}
     */
    getRelativeFilename: file => path.relative(DESTINATION_FOLDER, path.resolve(config.folders.root, file)),


    /**
     * Schedule a file update for later, if the file is allowed on the server.
     *
     * @param {string} file - The path and filename of the file that should be updated.
     * @param {string} eventType - The event type that fired this function. Really only used for aestetic purposes.
     */
    scheduleFileUpdate: (file, eventType) => {

        const relativeFilename = helpers.getRelativeFilename(file);

        if (helpers.checkIfSynchronizationIsAllowed(relativeFilename)) {

            log(`${colors.cyan(colors.bold(relativeFilename))} was ${colors[eventType === PAYLOAD_TYPES.CHANGE ? "yellow" : "green"](colors.bold(eventType))}`);

            payload[relativeFilename] = PAYLOAD_TYPES.CHANGE;

            schedulePayloadSend();

        }

    },


    /**
     * Schedule a file deletion for later, if the file is allowed to be deleted.
     *
     * @param {string} file - The file to delete on the server.
     */
    scheduleFileDeletion: file => {

        const relativeFilename = helpers.getRelativeFilename(file);

        if (helpers.checkIfSynchronizationIsAllowed(relativeFilename, true)) {

            log(`${colors.cyan(colors.bold(relativeFilename))} was ${colors.red(colors.bold(PAYLOAD_TYPES.DELETE))}`);

            if (payload[relativeFilename] && payload[relativeFilename] === PAYLOAD_TYPES.CREATE) {
                delete payload[relativeFilename];
            } else {
                payload[relativeFilename] = PAYLOAD_TYPES.DELETE;
            }

            schedulePayloadSend();

        }
    }


};


/**
 * The "sync everything"-task will upload everything in the destination folder to the server.
 *
 * @param {function} done - Callback function, passed in by Gulp.
 */
function syncEverything(done) {

    if (config.tasks.sxa && config.tasks.sxa.enable) {
        helpers.login().then(() => {
            const map = require("map-stream");
            const watchTarget = path.relative(config.folders.root, path.resolve(DESTINATION_FOLDER, '**/', config.tasks.sxa.watchFor));

            log(`${colors.green(`Starting complete sync from folder `)}${colors.cyanBright(path.relative(config.folders.root, path.resolve(DESTINATION_FOLDER)))}`);

            gulp.src(
                watchTarget,
                { cwd: path.resolve(config.folders.root), read: false }
            )
                .on("end", () =>
                    helpers.sendPayloads().then(() => {
                        log(colors.green(`Complete sync complete`));
                        done();
                    })
                )
                .pipe(map((file, cb) => {
                    const relativePath = path.relative(DESTINATION_FOLDER, file.path);
                    if (helpers.checkIfSynchronizationIsAllowed(relativePath)) {
                        payload[relativePath] = PAYLOAD_TYPES.CHANGE;
                    }
                    cb();
                }));

        });
    }
}


/**
 * A task for watching changes in the destination folder and conditionally uploading them to the server
 * when stuff changes.
 */
function watch() {

    if (config.tasks.sxa && config.tasks.sxa.enable) {
        helpers.login().then(() => {
            const watchTarget = path.relative(config.folders.root, path.resolve(DESTINATION_FOLDER, '**/', config.tasks.sxa.watchFor));

            gulp.watch(
                watchTarget,
                { cwd: path.resolve(config.folders.root) }
            )
                .on('add', file => helpers.scheduleFileUpdate(file, PAYLOAD_TYPES.CREATE))
                .on('change', file => helpers.scheduleFileUpdate(file, PAYLOAD_TYPES.CHANGE))
                .on('unlink', file => helpers.scheduleFileDeletion(file))
            ;

            log(`${colors.green(`Watcher running! Watch folder: `)}${colors.cyanBright(path.relative(config.folders.root, path.resolve(DESTINATION_FOLDER)))}`);
        });
    } else {
        log(colors.yellowBright("SXA tasks are not enabled. Please enable them in the configuration, and try again."));
        process.exit();
    }
}



// Only expose the SXA-related tasks if they are enabled in the configuration.
module.exports = config.tasks.sxa && config.tasks.sxa.enable ? {
    "sxa:watch": watch,
    "sxa:syncEverything": syncEverything
} : {};
