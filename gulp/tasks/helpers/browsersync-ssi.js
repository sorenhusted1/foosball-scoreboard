/*global __dirname */

// This is a copy of "browsersync-ssi", modified to not try to inject a wrong version of browsersync
// causing 404 errors. This copy also has a bugfix to make it work with URLs with queries.
// https://github.com/soenkekluth/browsersync-ssi
module.exports = function browserSyncSSI(options) {
    const SSI = require('ssi');
    const path = require('path');
    const fs = require('graceful-fs');

    const opt = options || {};
    const ext = opt.ext || '.shtml';
    const baseDir = opt.baseDir || __dirname;

    const parser = new SSI(baseDir, baseDir, `/**/*${ext}`);

    return (req, res, next) => {

        // Split the URL to get the path name and remove the query, so we can check the file name
        const url = req.originalUrl || req.url;
        const urlSplit = url.split('?');
        const pathName = urlSplit[0];
        const filename = path.join(baseDir, pathName.substr(-1) === '/' ? `${pathName}index${ext}` : pathName);

        // Parse the file if it has the correct extension
        if (filename.toLowerCase().indexOf(ext.toLowerCase()) > -1 && fs.existsSync(filename)) {

            // Parse the file
            const contents = parser.parse(filename, fs.readFileSync(filename, {
                encoding: 'utf8'
            })).contents;

            // Send the parsed file contents to browsersync
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            res.end(contents);

        }

        // The file didn't have the correct extension and will be ignored by this middleware
        else {
            next();
        }

    };
};
