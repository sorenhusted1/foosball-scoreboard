/*global global, __dirname */
const { config, gulp } = global.akqa;
const destMultiple = require('gulp-multi-dest');
const buildFolderCollection = require("../helpers/buildFolderCollection");
const notifier = require("node-notifier");
const path = require("path");
const defaultOutputFolders = buildFolderCollection(config.tasks.javascript.targetFolder);
const colors = require("ansi-colors");
const log = require("fancy-log");

let analyzeMode = false;


function errorHandler(err) {
    notifier.notify({
        title: 'JS compile error',
        message: err.message,
        icon: path.resolve(__dirname, '../icons/js.png')
    });

    log(colors.red("******************************************************************************"));
    if (err.loc) {
        const msg = `: ${err.message}`;
        log(`${colors.red(colors.bold(err.loc.file))}${colors.yellow(msg)}`);
    } else {
        log(colors.red(colors.bold(err.message)));
    }
    if (err.snippet) {
        log(colors.yellow(err.snippet));
    }
    log(colors.red("******************************************************************************"));

    if (global.akqa.production) {
        throw new Error(err.message);
    }

    this.emit('end');
}



let baseConfiguration;


/**
 * Build and return the base configuration. Done once and then cached so we
 * won't have to do it constantly.
 *
 * @private
 * @returns {object}
 */
function getBaseConfiguration() {

    if (!baseConfiguration) {
        const MiniCssExtractPlugin = require("mini-css-extract-plugin");
        const jsonSASSImporter = require("node-sass-json-importer");
        const scssPathImportHelper = require('../helpers/scssPathImportHelper');

        // Find out whether or not to build for production use.
        const buildForProduction = global.akqa.production || !config.disableMinificationDuringDevelopment || analyzeMode;

        // Set optimization parameters directly from the configuration.
        const optimization = config.tasks.javascript.codeSplittingConfiguration || {};

        // Begin an array containing all our Webpack-plugins. We will be using this later.
        const plugins = [];

        const webpackModuleRules = [];

        // Add the plugins you need for Babel to this array. Babel is configured just before the output stage.
        const babelPlugins = [
            ["@babel/proposal-class-properties"],
            ["@babel/proposal-object-rest-spread", { "useBuiltIns": true }]
        ];


        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // UGLIFY THE CODE FOR PRODUCTION
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        if (buildForProduction) {
            const TerserPlugin = require('terser-webpack-plugin');
            optimization.minimizer = [
                new TerserPlugin({
                    terserOptions: {
                        ecma: config.tasks.javascript.ecmaTargetVersion,
                        compress: {
                            ecma: config.tasks.javascript.ecmaTargetVersion,
                        },
                        output: {
                            "ecma": config.tasks.javascript.ecmaTargetVersion,
                            "ascii_only": true, // https://github.com/facebook/create-react-app/issues/2488
                            "beautify": false
                        }
                    }
                })
            ];
        }



        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // STYLES-IN-JS CONFIGURATION
        //
        // If you are not going to use CSS-in-JS, you can skip this entire section (the next ~200 lines or so). If you do
        // want to use it, you should probably familiarize yourself with what is possible in our pipeline. Basic
        // documentation is included below.
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************

        const processedCSSLocation = `../${config.tasks.scss.targetFolder}/${config.tasks.javascript.stylesInJS.outputFolderName}/`;




        // We want some configuration for both the CSS-loader and the SCSS-loader that we're configuring soon.
        const cssLoaderConfig = {
            minimize: buildForProduction,
            importLoaders: 1, // weird, yes? It's so PostCSS can work if CSS-modules are enabled.

            // Enable CSS modules or nah?
            modules: config.tasks.javascript.stylesInJS && config.tasks.javascript.stylesInJS.cssModulesEnabled,
            localIdentName: buildForProduction
                ? config.tasks.javascript.stylesInJS.classNamePatternProduction  || '[name]--[hash:base64:6]'
                : config.tasks.javascript.stylesInJS.classNamePatternDevelopment || '[path][name]__[local]--[hash:base64:6]'
        };



        const scssLoaderConfig = {
            includePaths: [path.resolve(config.folders.root, config.folders.sourceFolder, config.tasks.scss.sourceFolder)],
            importer: [jsonSASSImporter(), scssPathImportHelper],
            outputStyle: buildForProduction ? "compressed" : "compact"
        };


        // "PostCSS," I hear you thinking. "Isn't that mighty scary and nasty, and tricksy, and
        // we don't like it precioussss," and you would be almost correct. But the autoprefixer
        // is something we like very much, and the postcss-loader is by far the easiest/only
        // way to get it into our project. So... there. PostCSS is yes.
        const postCSSLoader = {
            loader: 'postcss-loader',
            options: {
                ident: 'postcss',
                plugins: [
                    require('autoprefixer')(config.tasks.scss.autoPrefixerConfig)
                ]
            }
        };


        let cssLoader;
        let scssLoader;


        if (config.tasks.javascript.stylesInJS.useDynamicIncludes) {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // If we're here, we're gonna export our CSS (and SCSS) to CSS-files in the CSS output folder (usually these
            // end up as "css/components/whatever.hashValue.css"), and the JS will include them dynamically when needed.
            // Please note that if you have enabled "useStyleInjectionInDevelopment", the styles will be injected as style
            // blobs during runtime - this is cool for fast testing, and possibly hot module replacement (if supported),
            // but you should test it in production mode as well.
            //
            //
            // PROS: This is really cool, and your initial stylesheets will be a lot smaller.
            //
            // CONS: BE AWARE THAT THIS MIGHT LEAD TO FOUC (Flash Of Unstyled Content) IF THE CONNECTION IS SLOW!
            //       You can alleviate this problem by setting the initial styles of your app to hidden inside your
            //       critical CSS, and then override it using the component styles here.
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // Style injection will be forced if CSS modules are enabled (because `style-loader/url` doesn't seem to work
            // with the CSS module syntax..?), or if we've enabled injection during development and production building
            // is disabled.
            const injectStyles = config.tasks.javascript.stylesInJS.cssModulesEnabled || (config.tasks.javascript.stylesInJS.useStyleInjectionInDevelopment && !buildForProduction);

            // Configure the basic loaders
            const fileLoader = { loader: "file-loader", options: { name: `${processedCSSLocation}${config.tasks.javascript.stylesInJS.dynamicFilename}` } };
            const styleLoaderName = injectStyles ? 'style-loader' : 'style-loader/url';

            // If we're not in production, and we want to inject the styles, use the css-loader. Otherwise use the file-loader.
            const loaderToUse     = injectStyles ? { loader: 'css-loader', options: cssLoaderConfig } : fileLoader;

            // Configure the CSS-loader.
            cssLoader = {
                test: /\.css$/,
                use: [
                    { loader: styleLoaderName, options: { sourceMap: !buildForProduction } },
                    loaderToUse,
                    postCSSLoader
                ]
            };

            // Configure the SCSS-loader.
            scssLoader = {
                test: /\.scss$/,
                use: [
                    { loader: styleLoaderName, options: { sourceMap: !buildForProduction } },
                    loaderToUse,
                    postCSSLoader,
                    {
                        loader: 'sass-loader',
                        options: scssLoaderConfig
                    },
                ]
            };



        } else {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // If we're here, we will process the styles like above, but we will NOT dynamically import the styles. Not
            // even if "useStyleInjectionInDevelopment" is enabled.
            //
            //
            // PROS: This prevents the FOUCs, because you will have to manually include the files in your viewfiles
            //       or wherever you are using this. Also, if any of your styling should be independent of JS-support,
            //       this is the way to go.
            //
            // CONS: You WILL have to manually include the files to see any of your styling.
            //
            //
            // HOWEVER: if you dynamically import code that imports styles, these will be dynamically imported automatically
            // as well. This means that any styling imported inside your main script will NOT be injected, but merely output
            // to a file (named like your entry point). Any styling imported inside files you import() dynamically will be
            // put into separate files and included dynamically at runtime.
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // Make sure we have a plugin ready to extract SCSS from our JS-files.
            const extractStylesPlugin = new MiniCssExtractPlugin({
                filename: `${processedCSSLocation}${config.tasks.javascript.stylesInJS.staticFilename}`
            });

            plugins.push(extractStylesPlugin);


            cssLoader = {
                test: /\.css$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader', options: cssLoaderConfig },
                    postCSSLoader
                ]
            };

            scssLoader = {
                test: /\.scss$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    {
                        loader: 'css-loader',
                        options: cssLoaderConfig
                    },
                    {
                        loader: 'sass-loader',
                        options: scssLoaderConfig
                    },
                    postCSSLoader
                ]
            };
        }



        // Our various Webpack loader modules need some rules, so let's set them now.
        webpackModuleRules.push(cssLoader);
        webpackModuleRules.push(scssLoader);






        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // SVG INLINE IMPORT LOADER
        // ... because loading SVGs as resources for use in the frontend is pretty damned awesome.
        //
        // If you need other file types, and they shouldn't pass through the SVG optimizer first, use the
        // "raw loader" instead: https://github.com/webpack-contrib/raw-loader
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************

        webpackModuleRules.push({
            test: /\.svg$/,
            loader: 'svg-inline-loader'
        });







        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // CODE SPLITTING CONFIGURATION
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************

        // We'll need a plugin for dynamically loading stuff "through" Babel. Really,
        // this is all that's needed to get insanely fine-grained code splitting with
        // Webpack 4. Crazy.
        babelPlugins.push('@babel/syntax-dynamic-import');




        // I 💗 you, javascript task.





        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // CODE TRANSPILING
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************

        // Time to add Babel. Because reasons.
        webpackModuleRules.unshift({
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ["@babel/env", config.tasks.javascript.babelPresetEnv],
                        "@babel/react"
                    ],
                    plugins: babelPlugins
                }
            }
        });





        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // CODE ANALYZER
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        if (analyzeMode) {
            const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
            plugins.push(new BundleAnalyzerPlugin({
                analyzerMode: 'disabled',
                generateStatsFile: true
            }));
        }



        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        //
        //
        // OUTPUT STAGE
        //
        //
        // ****************************************************************************************************************
        // ****************************************************************************************************************
        // ****************************************************************************************************************


        baseConfiguration = {
            // Generate a source map in line inside the JS-file if we're not building for production.
            devtool: !buildForProduction ? config.tasks.javascript.devSourceMapMode : false,
            mode: buildForProduction ? 'production' : 'development',
            plugins,
            optimization,
            resolve: {
                alias: {
                    "~": path.resolve(config.folders.root, config.folders.sourceFolder)
                }
            },
            output: {
                filename: config.tasks.javascript.outputFilename || "[name].min.js",
                // publicPath is needed for dynamically imported code. It should point to the folder on the server where we can find the "lazy" files.
                publicPath: config.folders.dynamicJSImportSource,
                chunkFilename: config.tasks.javascript.chunkFilename || "chunk.[name].[chunkhash:6].js",
                pathinfo: true,
                path: defaultOutputFolders[0]
            },
            module: {
                rules: webpackModuleRules
            }
        };
    }


    return baseConfiguration;

}





/**
 * Perform compilation of a single javascript file.
 *
 * @param {object} entrypoints - The files to process.
 * @returns {*}
 */
function doCompile(entrypoints) {

    const webpackStream = require("webpack-stream");

    const configuration = getBaseConfiguration();
    configuration.entry = entrypoints;


    // Okay, that's enough configuration. Go! GO GO GO!
    const pipe = webpackStream(configuration)
        .on('error', errorHandler) // Handle errors gracefully
        .pipe(destMultiple(defaultOutputFolders));


    // We can't use gulp-if here, since the instance might be undefined, which screws up everything.
    // This, however ugly, works fine.
    if (global.akqa.browsersyncInstance) {
        return pipe.pipe(global.akqa.browsersyncInstance.stream({match: '**/*.js'}));
    }

    return pipe;

}


function javascript() {

    const filesToRender = {};

    if (Array.isArray(config.tasks.javascript.entryPoints)) {

        config.tasks.javascript.entryPoints.forEach(entry =>
            filesToRender[entry.replace(/\.js$/, '')] = path.resolve(config.folders.root, config.folders.sourceFolder, config.tasks.javascript.sourceFolder, entry)
        );

    }

    else if (typeof config.tasks.javascript.entryPoints === "object") {
        const glob = require("glob");
        const baseFolder = path.resolve(config.folders.root, config.folders.sourceFolder, config.tasks.javascript.sourceFolder);

        const entrypointNames = Object.keys(config.tasks.javascript.entryPoints);
        entrypointNames.forEach(currentEntryPoint => {
            let files = [];

            // Let's run through the entrypoints one by one.
            config.tasks.javascript.entryPoints[currentEntryPoint].forEach(fileReference => {

                // Just in case the file reference is a glob (containing wildcards), we should try to resolve it
                // first, just to see what it contains.
                const globbedResult = glob.sync(fileReference, { cwd: baseFolder });

                // The results of that there thing will be an array of files. We'll concat the two arrays.
                files = files.concat(globbedResult.map(globbedFile => path.resolve(baseFolder, globbedFile)));

            });

            // Make sure the array now actually contains some resolved files, or we're gonna have words.
            if (!files.length) {
                throw `The entrypoint "${currentEntryPoint}" did not result in any files being resolved. Please check your globbing patterns, and that your source code exists.`;
            }

            filesToRender[currentEntryPoint] = files;
        });

    } else {
        throw "config.tasks.javascript.entryPoints must be an array of entry points, or an object! Please see the examples in the documentation.";
    }


    return doCompile(filesToRender);

}


function getPolyfills(done) {

    const polyfillTargetFolder = config.tasks.javascript.polyfillFallbackFetcher.targetFolder && config.tasks.javascript.polyfillFallbackFetcher.targetFolder !== config.tasks.javascript.targetFolder ? buildFolderCollection(config.tasks.javascript.polyfillFallbackFetcher.targetFolder) : defaultOutputFolders;

    const nodeFetch = require("node-fetch");
    const gulpFile = require("gulp-file");

    let urlToFetch = config.tasks.javascript.polyfillFallbackFetcher.url;


    // Add the various arguments to the fetch URL
    const args = config.tasks.javascript.polyfillFallbackFetcher.arguments;
    const implodedArguments = [];
    Object.keys(args).forEach(key => implodedArguments.push(`${key}=${encodeURIComponent(args[key])}`));
    urlToFetch += `?${implodedArguments.join("&")}`;


    // Print a nice message about what we're doing.
    log(`Fetching polyfills (features: ${colors.greenBright(config.tasks.javascript.polyfillFallbackFetcher.arguments.features.split(",").join(", "))}${config.tasks.javascript.polyfillFallbackFetcher.arguments.excludes ? `, ${colors.yellow(`excluding ${config.tasks.javascript.polyfillFallbackFetcher.arguments.excludes.split(",").join(", ")}`)}` : ''}) from ${colors.green(config.tasks.javascript.polyfillFallbackFetcher.url)} ...`);


    // Fetch the damned thing!
    nodeFetch(urlToFetch)
        .then(data => data.text())
        .then(scriptContent => {
            log(`Polyfill successfully fetched (${colors.greenBright(`${scriptContent.length} bytes`)}); writing to ${colors.greenBright(config.tasks.javascript.polyfillFallbackFetcher.targetFilename)}`);

            gulpFile(config.tasks.javascript.polyfillFallbackFetcher.targetFilename, scriptContent)
                .pipe(destMultiple(polyfillTargetFolder));

            done();
        })
        .catch(() => {
            log(colors.red(colors.bold(`Polyfill wasn't fetched! The service may not be responding, or you might not have internet access.`)));
            done();
        });

}




const setAnalyzeMode = done => {
    analyzeMode = true;
    done();
};



module.exports = {
    javascript,
    "javascript:analyze": gulp.series(setAnalyzeMode, javascript),
    "javascript:polyfills": getPolyfills
};
