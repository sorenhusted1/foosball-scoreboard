const { gulp, config } = global.akqa;
const path = require("path");
const destMultiple = require('gulp-multi-dest');
const buildFolderCollection = require("../helpers/buildFolderCollection");
const countFilesInDirectory = require('../helpers/countFilesInDirectory');
const colors = require("ansi-colors");
const log = require("fancy-log");


const svgFilePattern = "**/*.svg";



/**
 * @typedef {object} SVGProcessorSettingDefinition
 * @property {boolean} enable
 * @property {string} sourceFolder
 * @property {string} targetFolder
 * @property {boolean} removeIDsFromOutput
 * @property {string} [bundleName]
 * @property {object} [cheerio]
 * @property {boolean} [cheerio.removeDefs]
 */





/**
 * Process the given folder and optimize the SVG-files inside it.
 *
 * @param {SVGProcessorSettingDefinition} settings
 * @return {object}
 */
function optimizeSVGFiles(settings) {

    const svgmin = require("gulp-svgmin");

    const sourceFolder = path.resolve(config.folders.root, config.folders.sourceFolder, settings.sourceFolder);

    // We'll return the pipeline because we'll need it for further processing.
    return gulp
        .src(path.resolve(sourceFolder, svgFilePattern))
        .pipe(svgmin(file => {
            const prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [
                    {
                        cleanupIDs: {
                            prefix: `${prefix}-`,
                            minify: true,
                            remove: settings.removeIDsFromOutput
                        }
                    },
                    {
                        removeViewBox: false
                    }
                ]
            };
        }));

}


/**
 * Process invidividual SVG files.
 *
 * @param {SVGProcessorSettingDefinition} settings
 * @returns {Promise}
 */
function processIndividualFiles(settings) {

    const outputFolders = buildFolderCollection(settings.targetFolder);

    return new Promise(done =>
        optimizeSVGFiles(settings)
            .pipe(destMultiple(outputFolders))
            .on("finish", done)
    );

}



/**
 * Generate an SVG bundle.
 *
 * @param {SVGProcessorSettingDefinition} settings
 * @returns {Promise}
 */
function processBundle(settings) {

    const outputFolders = buildFolderCollection(settings.targetFolder);

    const rename = require("gulp-rename");
    const cheerio = require("gulp-cheerio");
    const svgstore = require("gulp-svgstore");

    return new Promise(done => {

        optimizeSVGFiles(settings)
            .pipe(svgstore())

            // Run the bundle through Cheerio in order to optimize it even further.
            .pipe(cheerio({
                parserOptions: { xmlMode: true },
                run: $ => {
                    // Remove "defs" definition from bundles, since that will just be straight messed up anyways.
                    if (settings.cheerio && settings.cheerio.removeDefs) {
                        $('defs').remove();
                    }

                    // You can add similar optimizations here if you wish.
                }
            }))

            .pipe(rename(settings.bundleName))
            .pipe(destMultiple(outputFolders))
            .on("finish", done);

    });

}




module.exports = () => {
    const promises = [];



    /*
     *****************************************************************************
     *****************************************************************************
     *****************************************************************************

     INDIVIDUAL FILES
     These are just optimized and put into the output folder.

     *****************************************************************************
     *****************************************************************************
     *****************************************************************************
     */
    if (typeof config.tasks.svg.individualFiles === "object" &&
        config.tasks.svg.individualFiles.enable &&
        config.tasks.svg.individualFiles.sourceFolder
    ) {

        promises.push(new Promise(individualProcessingComplete => {

            countFilesInDirectory(path.resolve(config.folders.root, config.folders.sourceFolder, config.tasks.svg.individualFiles.sourceFolder, svgFilePattern))
                .then(count => {

                    if (count) {
                        log(colors.green(`- Processing ${count} individual SVG-file${count === 1 ? '' : 's'}...`));
                        return processIndividualFiles(config.tasks.svg.individualFiles).then(() =>
                            log(colors.green(`- ${count} SVG-file${count === 1 ? '' : 's'} optimized.`))
                        );
                    }
                })
                .then(individualProcessingComplete)
            ;

        }));
    }





    /*
     *****************************************************************************
     *****************************************************************************
     *****************************************************************************

     BUNDLED FILES
     These are first optimised, and then run through an SVG bundler in order
     to get a single file.

     *****************************************************************************
     *****************************************************************************
     *****************************************************************************
     */
    if (typeof config.tasks.svg.bundle === "object" &&
        config.tasks.svg.bundle.enable &&
        config.tasks.svg.bundle.sourceFolder &&
        config.tasks.svg.bundle.bundleName
    ) {

        promises.push(new Promise(bundleProcessingComplete => {

            countFilesInDirectory(path.resolve(config.folders.root, config.folders.sourceFolder, config.tasks.svg.bundle.sourceFolder, svgFilePattern))
                .then(count => {
                    if (count) {
                        log(colors.green(`- Bundling ${count} SVG-file${count === 1 ? '' : 's'} as ${colors.cyan(config.tasks.svg.bundle.bundleName)}`));
                        return processBundle(config.tasks.svg.bundle).then(() =>
                            log(colors.green(`- SVG-bundle generated.`))
                        );

                    }
                })
                .then(bundleProcessingComplete)
            ;

        }));


    }



    // Return a promise that resolves once any and all processors are done doing their thing.
    return Promise.all(promises);

};
