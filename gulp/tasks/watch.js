const { gulp, config } = global.akqa;
const path = require("path");
const colors = require("ansi-colors");
const log = require("fancy-log");


const rootFolder = path.resolve(config.folders.root, config.folders.sourceFolder);

const setProduction = require('./helpers/setProduction');
const browserSync = require('./helpers/browsersync');


function watch() {

    const knowntasks = Object.keys(config.tasks);

    knowntasks.forEach(currentTaskID => {
        const currentTask = config.tasks[currentTaskID];
        if (currentTask.watch && currentTask.watchFor && (currentTask.sourceFolder || currentTask.watchFolders)) {

            let watchTarget;
            let negatedFolders = 0;
            const watchFolders = currentTask.watchFolders || currentTask.sourceFolder;

            if (watchFolders instanceof Array) {
                watchTarget = watchFolders.map(sourceFolderName => {

                    // Since the user is defining the watched folders as an array, we'll allow for more control - like,
                    // adding negation folder patterns so we can exclude folders from the watch. That's pretty neat.
                    const negationPattern = sourceFolderName[0] === "!";

                    if (negationPattern) {
                        negatedFolders += 1;
                    }

                    return (negationPattern ? "!" : "") +
                        // Using relative paths to ensure Gulp keeps an eye on newly added files as well as the ones it already knows.
                        path.relative(config.folders.root, path.resolve(rootFolder, (negationPattern ? sourceFolderName.substr(1) : sourceFolderName), '**/', currentTask.watchFor));
                });
            } else {
                // Using relative paths to ensure Gulp keeps an eye on newly added files as well as the ones it already knows.
                watchTarget = path.relative(config.folders.root, path.resolve(rootFolder, watchFolders, '**/', currentTask.watchFor));
            }

            const watchedFolders = typeof watchTarget === "string" ? 1 : watchTarget.length;
            const watchedFolderString = `(${watchedFolders} folder${watchedFolders === 1 ? '' : 's'})`;
            log(`${colors.green('- Adding watcher for task')} ${colors.cyan(currentTaskID)} ${colors.gray(watchedFolderString)}${negatedFolders ? colors.gray(` (${negatedFolders} excluded)`) : ''}`);

            gulp.watch(
                watchTarget,
                {
                    cwd: path.resolve(config.folders.root)
                },
                gulp.parallel(currentTask.watchRun || [currentTaskID])
            )
                .on('change', file => log(`${colors.green(colors.bold('✓'))} ${colors.cyan(colors.bold(file))} was ${colors.yellow(colors.bold('changed'))}`))
                .on('add', file => log(`${colors.green(colors.bold('✓'))} ${colors.cyan(colors.bold(file))} was ${colors.green(colors.bold('added'))}`))
                .on('unlink', file => log(`${colors.green(colors.bold('✓'))} ${colors.cyan(colors.bold(file))} was ${colors.red(colors.bold('removed'))}`))
            ;
        }
    });

    if (config.tasks.sxa && config.tasks.sxa.enable) {
        log(`${colors.green(colors.bold('✓ Source watchers are running, setting up SXA handling...'))}`);

        const sitecoreTasks = require("./sxa");
        sitecoreTasks["sxa:watch"]();

    } else {
        log(`${colors.green(colors.bold('✓ Watchers are running!'))}`);
    }
}


const watchAction = gulp.series(browserSync, watch);


module.exports = {
    "watch": watchAction,
    'watch:production': gulp.series(setProduction, watchAction)
};

