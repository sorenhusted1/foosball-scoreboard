import React from 'react';

import Score from './score';
import Winner from './winner';
import SetTeams from './setTeams';

const winnerScore = 10;
const teams = [
    'blue',
    'red',
];
const teamData = [
    {
        color: teams[0],
        name: '',
    },
    {
        color: teams[1],
        name: '',
    },
];
const initialState = {
    blueScore: 0,
    redScore: 0,
    hasEnded: false,
    setTeams: true,
    winnerTeam: '',
};

class ScoreBoardApp extends React.Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    componentWillUpdate(nextProps, nextState) {
        teams.map(team => {
            if (nextState[this.getTeamScore(team)] === winnerScore && nextState.hasEnded === this.state.hasEnded) {
                this.endGame(team);
            }
        });
    }

    getTeamScore = team => `${team}Score`;

    getOtherTeam = team => teams.indexOf(team) === 0 ? teams[1] : teams[0];

    reset = () => {
        this.setState(initialState);
    }

    score = team => {
        this.setState(prevState => ({
            [this.getTeamScore(team)]: prevState[this.getTeamScore(team)] + 1
        }));
    }

    endGame = winnerTeam => {
        this.setState({
            hasEnded: true,
            winnerTeam,
        });
    }

    setTeamNames = names => {
        event.preventDefault();
        teamData[0].name = names.blue;
        teamData[1].name = names.red;
        this.setState({
            setTeams: false
        });
    }

    render() {
        const { hasEnded, winnerTeam, setTeams } = this.state;

        return (
            <>
                { setTeams && (
                    <SetTeams
                        teams={ teams }
                        setTeamNames={ this.setTeamNames }
                    />
                )}
                { !hasEnded
                    ? (
                        <div className="score-container">
                            { teamData.map(({name, color}) => (
                                <Score
                                    name={ name }
                                    color={ color }
                                    score={ this.state[this.getTeamScore(color)] }
                                    key={ color }
                                >
                                    {/* Manual goal controls */}
                                    <button onClick={ () => this.score(color) }>Add goal to team { color }</button>
                                </Score>
                            )) }
                        </div>
                    )
                    : (
                        <Winner
                            winnerTeam={ winnerTeam }
                            winnerTeamScore={ this.state[this.getTeamScore(winnerTeam)] }
                            loserTeamScore={ this.state[this.getTeamScore(this.getOtherTeam(winnerTeam))] }
                        />
                    )
                }
                <button onClick={this.reset}>Reset</button>
            </>
        );
    }
}

export default ScoreBoardApp;
