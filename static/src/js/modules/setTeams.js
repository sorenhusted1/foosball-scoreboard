import React from 'react';

class SetTeams extends React.Component {
    handleTeamName = (event, team) => {
        this.setState({
            [team]: event.target.value
        });
    }

    render() {
        const {
            teams,
            setTeamNames,
        } = this.props;

        return (
            <form onSubmit={() => setTeamNames(this.state)}>
                { teams.map(team => (
                    <div key={ team }>
                        <input type="text" name={`team-name-${team}`} onChange={ e => this.handleTeamName(e, team)}/>
                    </div>
                ))}
                <button type="submit">Go!</button>
            </form>
        );
    }
}

export default SetTeams;
