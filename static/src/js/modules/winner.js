import React from 'react';

const Winner = ({
    winnerTeam,
    winnerTeamScore,
    loserTeamScore,
}) => (
    <div className="winner">
        Team <span className={ `winner__team winner__team--${winnerTeam}` }>{ winnerTeam }</span> has won { winnerTeamScore } over { loserTeamScore }
    </div>
);

export default Winner;
