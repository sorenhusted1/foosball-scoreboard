import React from 'react';

const Score = ({
    name,
    color,
    score,
    children,
}) => (
    <div className="score">
        <h2 className={`score__title score__title--${color}`}>Team { name }</h2>
        <div className="score__amount">{ score }</div>
        { children }
    </div>
);

export default Score;
