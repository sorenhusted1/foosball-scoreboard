import "./utils/dom/imageHandler";
import { onReady } from "./utils/events/onReady";
import { bootstrapper } from "./utils/bootstrapper";
import React from 'react';
import ReactDOM from 'react-dom';
import ScoreBoardApp from "./modules/app";



// Enable this if you use <svg><use> tags. But please don't - there are better solutions!
// import svg4everybody from 'svg4everybody';



// Bootstrap the modules. You can add as many modules as you want, as indicated by the commented lines.
// Please do not make multiple calls to bootstrapper()! One call with all the references will be MUCH faster.
// For more information, and alternate bootstrapping methods, look inside bootstrapper.js and check out the
// various exported methods.
bootstrapper({

});



/*
// If you have a LOT of modules, and not all of them are included on every page, you
// might want to fully split your code to dynamically load the modules only when they're
// discovered on the page. Please make sure you're using HTTP/2, or the resulting load
// could potentially create a whole bunch of HTTP-requests that might choke a mobile
// device. This technique is best suited for modules that MIGHT show up on the page,
// but for modules that are GUARANTEED to show up everywhere, you should probably
// use the regular bootstrapper - or, if there's only a single usage, just turn it into
// a singleton instead.

// If you do this, obviously don't also import the modules at the top of this file.

asyncBootstrapper({
    "example": () => import("./modules/example").then(module => module.Example),
    "example2": () => import("./modules/example2").then(module => module.SomeOtherConstructor),
    "otherModule": () => import("./modules/otherModule").then(module => module.YetAnotherConstructor),
    "krombopulosMichael": () => import("./modules/krombopulosMichael").then(module => module.OohBoyHereIGoKillingAgain),
});

*/



/*
// If you want to use React instead of vanilla modules, use the reactBootstrapper,
// the same rules and guidelines apply for this bootstrapper as they do for the bootstrapper and asyncBootstrapper (split your code responsibly).
// The reactBootstrapper supports both static and dynamic imports - which is why there is no asyncReactBootstrapper
import reactBootstrapper from "./utils/react/reactBootstrapper";
import Example from "./modules/react/example";
const Example2 = import("./modules/react/example2");
reactBootstrapper({
    Example,
    Example2,
});




*/

// Run secondary functions at low priority, giving our own modules time to instantiate first.
onReady(() => {
    ReactDOM.render(<ScoreBoardApp />, document.getElementById('app'));
}, 150);


// You can use the following code to listen for when the styles on the page are lazy-loaded. This is cool for waiting for
// styles to be rendered and ready (if you need them for calculating sizes and similar), but since everything is loaded
// asynchronously, you can't know the load order, so the CSS might be loaded before the JS is. Your code MUST assume that
// the CSS has *already* been loaded once the JS runs.
// This function is especially helpful for slow browsers (IE) and slow/unstable connections.
//
// window.akqa = window.akqa || {};
// window.akqa.stylesRendered = () => {
//     // Run your repaint callbacks here. Consider using a throttle/debounce, and
//     // remember to run the function at load-time if the code here is important
//     // for your layout (pro-tip: it probably shouldn't be).
// };
