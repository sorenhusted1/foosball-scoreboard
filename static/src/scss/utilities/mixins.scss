﻿//
// Mixins
// --------------------------------------------------

// Utilities
// -------------------------

/// Clean a button of its default browser styling.
/// @group mixins and tools
/// @example
///   button {
///       @include clean-button;
///       color: blue;
///       text-decoration: underline;
///   }
@mixin clean-button {
    appearance: none;
    background: transparent;
    border: 0;
    border-radius: 0;
    cursor: pointer;
    outline: none;
    padding: 0;
    -webkit-tap-highlight-color: transparent;
    user-select: none;
    vertical-align: bottom;
}

/// Apply clearfix to an element.
/// @group mixins and tools
/// @example
///   .parent {
///       @include clearfix;
///
///       .child {
///           float: right;
///           height: 300px;
///       }
///   }
@mixin clearfix {
    &::before,
    &::after {
        content: " ";
        display: table;
    }

    &::after {
        clear: both;
    }
}

/// Prepend a selector (like a tag name) to the last part of the selector
///
/// @group mixins and tools
/// @since 3.5.0
/// @author Lars Munkholm <lars.munkholm@akqa.com>
/// @param {string} $prepend - The selector to prepend
/// @param {number} $goBack [0] - How many parents to go back
///
/// @example scss
///   form .input {
///       @include prepend-selector("input") { height: 50px; }
///       @include prepend-selector("textarea") { height: 200px; }
///   }
///
///   // This will output:
///   form input.input { height: 50px; }
///   form textarea.input { height: 200px; }
///
/// @example scss - Use the "goBack" parameter to match a parent's tag name.
///   .content .ribbon {
///       @include prepend-selector("article", 1) { background: white; }
///       @include prepend-selector("section", 1) { background: grey; }
///   }
///
///   // This will output:
///   article.content .ribbon { background: white; }
///   section.content .ribbon { background: grey; }
///
/// @example scss - Use **strings** to prepend classes or even create something like a modifier using "&".
///   .box img {
///       @include prepend-selector(".box--small", 1) { height: 100px; }
///       @include prepend-selector("&--large", 1) { height: 200px; }
///   }
///
///   // This will output:
///   .box--small.box img { height: 100px; }
///   .box--large img { height: 200px; }
@mixin prepend-selector($prepend, $goBack: 0) {
    @if str-index($prepend, ":") == 1 {
        $prepend: "&" + $prepend;
    }

    @each $selector in & {
        $result: null;
        $goBackThis: $goBack;

        // Go back a step further for each sibling combinator
        @for $i from 1 through length($selector) {
            $thisSelector: nth($selector, $i);

            @if $thisSelector == ">" or $thisSelector == "+" or $thisSelector == "~" {
                $goBackThis: $goBackThis + 1;
            }
        }

        $target: length($selector) - $goBackThis;

        // We've gone too far!
        @if $target < 1 {
            @debug "@mixin prepend-selecor: The $goBack parameter is set to #{$goBack} which is too much for #{$selector}";
        }

        // Create a new selector
        @else {
            @for $i from 1 through length($selector) {
                @if $i == $target {
                    $thisSelector: nth($selector, $i);
                    $prependOffset: str-index($thisSelector, ":") or str-length($thisSelector) + 1;
                    $prependSelector: str-replace($prepend, "&", str-slice($thisSelector, 1, $prependOffset - 1)) + str-slice($thisSelector, $prependOffset);

                    @if $prependSelector == $prepend {
                        $result: append($result, $prependSelector + $thisSelector);
                    } @else {
                        $result: append($result, $prependSelector);
                    }
                } @else {
                    $result: append($result, nth($selector, $i));
                }
            }

            // Insert content under the new selector
            @at-root #{$result} {
                @content;
            }
        }
    }
}

// WebKit-style focus
@mixin tab-focus {
    // Default
    outline: thin dotted #333;

    // WebKit
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
}

// Placeholder text
@mixin placeholder($color: $color__input-placeholder) {
    // Firefox 19+
    &::-moz-placeholder {
        color: $color;

        @content;
    }

    // Safari and Chrome
    &::-webkit-input-placeholder {
        color: $color;

        @content;
    }

    // Internet Explorer 10+
    &:-ms-input-placeholder {
        color: $color;

        @content;
    }
}

/// Visually hide an element, but keep it accessible to screen readers.
/// @group mixins and tools
@mixin visuallyhidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}


// GRADIENTS
// --------------------------------------------------

// Horizontal gradient, from left to right
//
// Creates two color stops, start and end, by specifying a color and position for each color stop.
// Color stops are not available in IE9 and below.
@mixin gradient-horizontal($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
    background-image: linear-gradient(to right, $start-color $start-percent, $end-color $end-percent); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
    background-repeat: repeat-x;
}

// Vertical gradient, from top to bottom
//
// Creates two color stops, start and end, by specifying a color and position for each color stop.
// Color stops are not available in IE9 and below.
@mixin gradient-vertical($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
    background-image: linear-gradient(to bottom, $start-color $start-percent, $end-color $end-percent); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
    background-repeat: repeat-x;
}

@mixin gradient-directional($start-color: #555, $end-color: #333, $deg: 45deg) {
    background-image: linear-gradient($deg, $start-color, $end-color); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
    background-repeat: repeat-x;
}

@mixin gradient-horizontal-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
    background-image: linear-gradient(to right, $start-color, $mid-color $color-stop, $end-color);
    background-repeat: no-repeat;
}

@mixin gradient-vertical-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
    background-image: linear-gradient($start-color, $mid-color $color-stop, $end-color);
    background-repeat: no-repeat;
}

@mixin gradient-radial($inner-color: #555,$outer-color: #333) {
    background-image: radial-gradient(circle, $inner-color, $outer-color);
    background-repeat: no-repeat;
}

@mixin gradient-striped($color: rgba(255,255,255,.15),$angle: 45deg) {
    background-image: linear-gradient($angle, $color 25%, transparent 25%, transparent 50%, $color 50%, $color 75%, transparent 75%, transparent);
}



/// Media query manager (widths).
/// @group mixins and tools
/// @author Anders Gissel <anders.gissel@akqa.com>
/// @author Lars Munkholm <lars.munkholm@akqa.com>
/// @param {string} $breakpoint1 - First breakpoint
/// @param {string} $breakpoint2 [null] - Second breakpoint
/// @example
///   @include mq(sm) {
///       font-size: 1.5em;
///   }
/// @example
///   @include mq(md-max) {
///       // Put styling here, that will be applied when the width of the browser window
///       // is less than the width of "md" (which probably means under 992px)
///   }
/// @example
///   @include mq(sm, md-max) {
///       // Put styling here, that will be applied when the width of the browser window
///       // is more than or equal to the width of "sm" and less than that of "md".
///       // This probably means a width between (and including) 768px and 991px.
///   }
/// @requires {variable} $widthBreakpoints
/// @output A media-query with a min-width or a max-width.
@mixin mq($breakpoint1, $breakpoint2: null) {
    $sizeString1: get-breakpoint($breakpoint1);
    $sizeString2: '';

    @if $breakpoint2 {
        @if (str-index($breakpoint1, 'max') == null and str-index($breakpoint2, 'max') != null) or
            (str-index($breakpoint1, 'max') != null and str-index($breakpoint2, 'max') == null) {
            $sizeString2: get-breakpoint($breakpoint2);

            @if $sizeString2 {
                $sizeString2: ' and #{$sizeString2}';
            }
        } @else {
            @warn "It makes no sense to have two `min-width` or two `max-width` in the same media-quiery.";
        }
    }

    @if $sizeString1 {
        @media #{$sizeString1 + $sizeString2} {
            @content;
        }
    }
}

/// Media query manager (heights).
///
/// @group mixins and tools
/// @since 3.6.0
/// @author Anders Gissel <anders.gissel@akqa.com>
/// @author Lars Munkholm <lars.munkholm@akqa.com>
/// @param {string} $breakpoint1 - First breakpoint
/// @param {string} $breakpoint2 [null] - Second breakpoint
/// @example
///   @include mq-height(sm) {
///       font-size: 1.5em;
///   }
/// @example
///   @include mq-height(md-max) {
///       // Put styling here, that will be applied when the height of the browser window
///       // is less than the height of "md" (which probably means under 992px)
///   }
/// @example
///   @include mq-height(sm, md-max) {
///       // Put styling here, that will be applied when the height of the browser window
///       // is more than or equal to the height of "sm" and less than that of "md".
///       // This probably means a height between (and including) 768px and 991px.
///   }
/// @requires {variable} $heightBreakpoints
/// @output A media-query with a min-height or a max-height.
@mixin mq-height($breakpoint1, $breakpoint2: null) {
    $sizeString1: get-breakpoint($breakpoint1, height);
    $sizeString2: '';

    @if $breakpoint2 {
        @if (str-index($breakpoint1, 'max') == null and str-index($breakpoint2, 'max') != null) or
            (str-index($breakpoint1, 'max') != null and str-index($breakpoint2, 'max') == null) {
            $sizeString2: get-breakpoint($breakpoint2, height);

            @if $sizeString2 {
                $sizeString2: ' and #{$sizeString2}';
            }
        } @else {
            @warn "It makes no sense to have two `min-height` or two `max-height` in the same media-quiery.";
        }
    }

    @if $sizeString1 {
        @media #{$sizeString1 + $sizeString2} {
            @content;
        }
    }
}

@function is-length($value) {
    @return type-of($value) != "null" and (str-slice($value + "", 1, 4) == "calc" or index(auto inherit initial 0, $value) or type-of($value) == "number" and not unitless($value));
}

@function unpack($shorthand) {
    @if length($shorthand) == 1 {
        @return nth($shorthand, 1) nth($shorthand, 1) nth($shorthand, 1) nth($shorthand, 1);
    }

    @else if length($shorthand) == 2 {
        @return nth($shorthand, 1) nth($shorthand, 2) nth($shorthand, 1) nth($shorthand, 2);
    }

    @else if length($shorthand) == 3 {
        @return nth($shorthand, 1) nth($shorthand, 2) nth($shorthand, 3) nth($shorthand, 2);
    }

    @else {
        @return $shorthand;
    }
}


// Shorten position styling
// @include position(absolute, 20px 40px 60px 80px);
// Turns into:
// position: absolute;
// top: 20px;
// right: 40px;
// bottom: 60px;
// left: 80px;
@mixin position($position: relative, $coordinates: null null null null) {
    @if type-of($position) == list {
        $coordinates: $position;
        $position: relative;
    }

    $coordinates: unpack($coordinates);
    $offsets: (top: nth($coordinates, 1), right: nth($coordinates, 2), bottom: nth($coordinates, 3), left: nth($coordinates, 4));

    position: $position;

    @each $offset, $value in $offsets {
        @if is-length($value) {
            #{$offset}: $value;
        }
    }
}


/// Set aspect ratio as `padding-top`
/// @param {umber} $x - width
/// @param {umber} $y - height
/// @example @include ratio(16, 9);
@mixin ratio($x, $y) {
    padding-top: percentage($y / $x);
}



/// Calculates width of element if it needs to fill the whole column __and__ stretch to the edge of screen.
/// Can also stretch 'through' columns, that stand between our element and its one true love: the edge of the screen.
///
/// Needs to be a direct child of a column from our grid.
///
/// Needs 'overflow-x: hidden' on a full window width parent element (or even body or html)
/// otherwise horizontal scrollbar appears, due to the nature of VW that includes scrollbar width in its calculation on Windows OS.
///
/// This only calculates width. Layout (ie. absolute, float..) needs to be added accordingly.
///
/// @since 3.6.5
/// @group mixins and tools
/// @author Leo Karsbæk <leo.karsbaek@akqa.com>
///
/// @param {number} $columnWidth - the width of the column which the element is child of ie. col--sm-8
/// @param {number} $columnOffset [0] - the width of the columns which are between our target col, and edge of row.
/// @example html - How markup might look like:
///   <div class="grid grid-container">
///       <div class="row">
///           <div class="col col--sm-4 col--stretch">
///               <div class="bg-imagehandler stretch-armstrong">...</div>
///           </div>
///           <div class="col col--sm-2">
///               ...
///           </div>
///           <div class="col col--sm-1 col--stretch">
///               <div class="funky-gradient stretch-a-lot">...</div>
///           </div>
///           <div class="col col--sm-5">
///               ...
///           </div>
///       </div>
///   </div>
/// @example scss - Stretch our targets
///   .col--stretch {
///       position: relative;
///   }
///
///   .stretch-armstrong {
///       @include stretchColumn(4);
///       @include position(absolute, 0 0 0 auto);
///   }
///
///   .stretch-a-lot {
///       @include stretchColumn(1,5);
///       @include position(absolute, 0 auto 0 0);
///   }
@mixin stretchColumn($columnWidth, $columnOffset: 0) {
    // All percentageWidths are relative to the column width of the column which our element is child of
    $percentageWidthOfOneColumn: 100% / $columnWidth;
    $numberOfRemainingColumns: $grid__columns - $columnWidth;
    $percentageWidthOfRemainingColumns: $percentageWidthOfOneColumn * $numberOfRemainingColumns;
    $percentageWidthOfOffsetColumns: $percentageWidthOfOneColumn * $columnOffset;
    $percentageWidthOfRow: $percentageWidthOfOneColumn * $grid__columns;

    // 100vw - widthOfRemainingColumns + widthOfOffsetColumns - distanceFromRowEdgeToScreenEdge = distanceFromRowEdgeToScreenEdge + widthOfColumn
    width: calc((100vw - #{$percentageWidthOfRemainingColumns}) + #{$percentageWidthOfOffsetColumns} - ((100vw - #{$percentageWidthOfRow}) / 2));
}


/// Mixin for building a column definition for a given breakpoint. You should only use this in circumstances where
/// you want to build "weird" column definitions that fall outside the regular grid and pre-defined breakpoints.
///
/// @group mixins and tools
/// @author Christian Broström <christian.brostrom@akqa.com>
/// @author Elise Bach <elise.bach@akqa.com>
/// @author Anders Gissel <anders.gissel@akqa.com>
/// @author Dennis Haulund Nielsen <dennis.nielsen@akqa.com>
///
/// @example scss - Build custom grid definitions
///   @include buildColumn(derp, 6);
///   @include buildColumn(derp, 9);
///   @include buildColumn(derp, 12);
///   // This will result in these classes being built:
///   // .col--derp-6, .col--derp-9, .col--derp-12
///   // .col--offset-derp-6, .col--offset-derp-9, .col--offset-derp-12
///
/// @example html - How to use the regular grid:
///   <div class="grid grid-container">
///       <div class="row">
///           <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
///               This is a regular "cell".
///           </div>
///           <div class="col col--xxs-6 col--xs-4 col--offset-sm-3 col--sm-9">
///               This is a "cell" with offset added on the "sm"-breakpoint.
///           </div>
///       </div>
///   </div>
@mixin buildColumn($breakpointName, $columnWidth) {

    .col {
        @extend %baseColumn;

        // Only render the "--no-pad" flavor if padding is not disabled completely.
        @if (not $grid__disable-padding) {
            &--no-pad {
                @extend %baseColumnNoPad;
            }
        }
    }

    // Generates classes like col--md-5, col--sm-3, col--lg-7, etc. (".col--breakpointName-columnWidth").
    .col--#{$breakpointName}-#{$columnWidth} {
        @include mq($breakpointName) {
            // Calculate width as a percentage of the whole.
            $percentage: percentage($columnWidth / $grid__columns);
            flex: 0 0 $percentage;
            max-width: $percentage; // Fix for IE
        }
    }

    // Create horizontal offset classes.
    // Generates classes like col--offset-md-x, col--offset-sm-x, col--offset-lg-x (where x is a an integer).
    .col--offset-#{$breakpointName}-#{$columnWidth} {
        @extend %baseColumnOffset;

        @include mq($breakpointName) {
            // Calculates percentage based on $key-value and number of columns times 100.
            margin-left: percentage($columnWidth / $grid__columns);
        }
    }
}
